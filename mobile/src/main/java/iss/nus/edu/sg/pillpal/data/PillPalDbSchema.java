package iss.nus.edu.sg.pillpal.data;

/**
 * Created by Samrat on 13/5/16.
 */
public class PillPalDbSchema {

    /**
     * The Pill table
     */
    public static final class PillTable {

        public static final String NAME = "Pill";

        public static final class Cols {
            public static final String ID = "id";
            public static final String NAME = "name";
            public static final String DETAILS = "details";
            public static final String UNIT = "unit";
            public static final String THRESHOLD_EXPIRY = "thresholdExpiry";
            public static final String THRESHOLD_QUANTITY = "thresholdQuantity";
        }
    }

    /**
     * The Batch table. Each pill has a 1..* batches.
     */
    public static final class BatchTable {

        public static final String NAME = "Batch";

        public static final class Cols {
            public static final String ID = "id";
            public static final String PILL_ID = "pillId";
            public static final String EXPIRY_DATE = "expiry";
            public static final String QUANTITY = "quantity";
        }
    }

    /**
     * The Profile table.
     */
    public static final class ProfileTable {

        public static final String NAME = "Profile";

        public static final class Cols {
            public static final String ID = "id";
            public static final String NAME = "name";
            public static final String ALLERGY = "allergy";
            public static final String AGE= "age";
        }
    }

    /**
     * The Prescription table.
     */
    public static final class PrescriptionTable {

        public static final String NAME = "Prescription";

        public static final class Cols {
            public static final String ID = "id";
            public static final String PROFILE_ID = "profileId";
            public static final String EXPIRY_DATE = "expiryDate";
            public static final String DOCTOR_NAME = "doctorName";
            public static final String CLINIC_NAME = "clinicName";
            public static final String PRESCRIPTION_DATE = "prescriptionDate";
        }
    }

    /**
     * The Prescription Item table.
     */
    public static final class PrescriptionItemTable {

        public static final String NAME = "PrescriptionItem";

        public static final class Cols {
            public static final String ID = "id";
            public static final String PRESCRIPTION_ID = "prescriptionId";
            public static final String PILL_ID = "pillId";
            public static final String DOSAGE = "dosage";
            public static final String ROUTINE = "pillRoutine";
            public static final String INSTRUCTIONS = "instructions";
        }
    }
}
