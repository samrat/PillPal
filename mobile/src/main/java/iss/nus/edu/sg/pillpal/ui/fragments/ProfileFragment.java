package iss.nus.edu.sg.pillpal.ui.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.interfaces.IProfileManager;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.ui.activities.AddProfileActivity;
import iss.nus.edu.sg.pillpal.ui.activities.PrescriptionActivity;
import iss.nus.edu.sg.pillpal.utilities.DefaultDividerItemDecoration;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;

/**
 * Created by Koushik
 */
public class ProfileFragment extends Fragment {

    LinearLayout profileLayout;

    LinearLayout deleteLayout;

    public enum IntentMessage {
        Profile, Prescription;
    }

    IntentMessage intentMessage;

    public ProfileFragment() {
    }

    /********************************************************/
    // Instance Variables
    /********************************************************/
    @Bind(R.id.profile_recycler_view)
    RecyclerView mProfileRecyclerView;
    @Bind(R.id.imageButton)
    FloatingActionButton myFab;

    private ProfileAdapter mProfileAdapter;
    /********************************************************/
    // Fragment Lifecycle

    /********************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the base view
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        // Bind the butterknife library now
        ButterKnife.bind(this, view);
        // Set the layout manager
        mProfileRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Now wire up the recycler view
        updateUI();
        // Now add the home action
        addButtonActions();



        return view;
    }


    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        addHardwareBackButtonAction();
    }
    /********************************************************/
    // Helper Methods
    /********************************************************/
    /**
     * Method to wire up the recycler view. Also updates the other UI elements.
     */
    private void updateUI() {
        IProfileManager profileManager = ProfileManager.get(getActivity());
        List<Profile> profileList = profileManager.getAllProfiles();
        mProfileAdapter = new ProfileAdapter(profileList);
        mProfileRecyclerView.setAdapter(mProfileAdapter);
        mProfileRecyclerView.addItemDecoration(new DefaultDividerItemDecoration(getActivity()));

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {


            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.parseColor("#ff3232"));
                xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_clear_24dp);
                xMark.setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                ProfileAdapter testAdapter = (ProfileAdapter)recyclerView.getAdapter();
                testAdapter.setUndoOn(true);
                if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                ProfileAdapter adapter = (ProfileAdapter)mProfileRecyclerView.getAdapter();
                boolean undoOn = adapter.isUndoOn();
                if (undoOn) {
                    adapter.pendingRemoval(swipedPosition);
                } else {
                    adapter.remove(swipedPosition);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;



                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        itemTouchHelper.attachToRecyclerView(mProfileRecyclerView);
    }

    /**
     *
     *
     * Add actions to the various elements of the screens.
     *
     *
     */
    private void addButtonActions() {


        // Floating action button
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddProfileActivity.class);
                startActivity(i);
                NavigationUtility.animateLeft(getActivity());
            }
        });
    }


    /**
     * Method to override the hardware button action.
     */
    private void addHardwareBackButtonAction() {

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finishActivityWithAnimation();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    /**
     * Method to finish the activity with animation.
     */
    private void finishActivityWithAnimation() {
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
    }
    /********************************************************/
    // Recycler View Holder

    /********************************************************/
    private class ProfileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /********************************************************/
        // ProfileHolder - Instance Variables
        /********************************************************/

        private TextView mName;
        private TextView mAge;
        private TextView mAllergy;
        private Button btnDelete;
        private ImageView FirstLetterView;
        LinearLayout delLayout;
        LinearLayout normalLayout;





        /********************************************************/
        // ProfileHolder - Constructor

        /********************************************************/
        public ProfileHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.list_item_profile_name);
            mAge = (TextView) itemView.findViewById(R.id.list_item_profile_age);
            mAllergy = (TextView) itemView.findViewById(R.id.list_item_profile_allergy);
            btnDelete = (Button) itemView.findViewById(R.id.list_item_profile_delete_button);
            profileLayout = (LinearLayout) itemView.findViewById(R.id.individual_profile);
            FirstLetterView  =(ImageView)itemView.findViewById(R.id.first_letter_view);
            delLayout = (LinearLayout) itemView.findViewById(R.id.delete_layout);
            normalLayout = (LinearLayout) itemView.findViewById(R.id.normal_layout);
            btnDelete.setOnClickListener(this);



        }

        /********************************************************/
        // ProfileHolder - Public Methods
        /********************************************************/

        /**
         * Method to bind the profile details to the view.
         *
         * @param profile The profile which will be used to bind the view.
         */
        public void bindProfile(Profile profile) {
            mName.setText(profile.getName());
            mAge.setText("Age: " + Integer.toString(profile.getAge()));
            mAllergy.setText("Allergy: " + profile.getAllergy());
            btnDelete.setTag(profile.getId());
            profileLayout.setTag(profile.getId());
/********************
 *
 * UI MODIFICATION LIST BY DEEPAK
 *
 *
 *
 **********************/

            Typeface font = Typeface.createFromAsset(
                    itemView.getContext().getAssets(),
                    "Fonts/SegoeSemibold.ttf");
            mName.setTypeface(font);
            mAge.setTypeface(font);
            mAllergy.setTypeface(font);



            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color1 = generator.getColor("FFFFFF");
            TextDrawable.IBuilder builder = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .roundRect(2);
            String firstLetter = profile.getName().substring(0, 1);
            TextDrawable ic1 = builder.build(firstLetter, color1);
            FirstLetterView.setImageDrawable(ic1);



        }

        /********************************************************/
        // ProfileHolder - Interface Implementation

        /********************************************************/
        @Override
        public void onClick(View v) {

            final int profileId = (int) v.getTag();
            Log.v("ProfileIds", Integer.toString(profileId));

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Are you sure you want to delete this profile");
            alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Log.v("ProfileId", Integer.toString(profileId));
                    IProfileManager profileManager = ProfileManager.get(getActivity());
                    profileManager.deleteProfile(profileId);
                    // Update the UI now.
                    mProfileAdapter.setProfileList(ProfileManager.get(getActivity()).getAllProfiles());
                    mProfileAdapter.notifyDataSetChanged();
                }
            });

            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Action not required.
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }


    /********************************************************/
    // Recycler View Adapter

    /********************************************************/

    private class ProfileAdapter extends RecyclerView.Adapter<ProfileHolder> {

        private static final int PENDING_REMOVAL_TIMEOUT = 5000; // 3sec

        List<Profile> itemsPendingRemoval;
        int lastInsertedIndex; // so we can add some more items for testing purposes
        boolean undoOn; // is undo on, you can turn it on from the toolbar menu

        private Handler handler = new Handler(); // hanlder for running delayed runnables
        HashMap<Profile, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be



        /********************************************************/
        // ProfileAdapter - Instance Variables
        /********************************************************/
        // Instance variables
        private List<Profile> mProfileList;

        /********************************************************/
        // ProfileAdapter - Getter & Setters

        /********************************************************/
        public void setProfileList(List<Profile> profileList) {
            mProfileList = profileList;
        }

        /********************************************************/
        // ProfileAdapter - Constructor

        /********************************************************/
        // Constructor
        public ProfileAdapter(List<Profile> profileList) {

            itemsPendingRemoval = new ArrayList<>();


            mProfileList = profileList;
        }

        /********************************************************/
        // ProfileAdapter - Abstract method implementation

        /********************************************************/
        @Override
        public ProfileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_profile, parent, false);
            if (!(getActivity().getIntent().getStringExtra("Profile") == null) && getActivity().getIntent().getStringExtra("Profile").equals("Profile")) {
                intentMessage = IntentMessage.Profile;
            } else if (!(getActivity().getIntent().getStringExtra("Prescription") == null) && getActivity().getIntent().getStringExtra("Prescription").equals("Prescription")) {
                intentMessage = IntentMessage.Prescription;
            }
            //   Log.v("IntentMsgis",intentMessage.toString());
            if (intentMessage != null) {
                switch (intentMessage) {
                    case Prescription:
                        myFab.hide();
                        profileLayout = (LinearLayout) view.findViewById(R.id.individual_profile);
                        deleteLayout = (LinearLayout) view.findViewById(R.id.delete_layout);
                        deleteLayout.setVisibility(View.GONE);

                        //ImageView btnDelete = (ImageView) view3.findViewById(R.id.list_item_profile_delete_button);
                        //  btnDelete.setVisibility(view2.INVISIBLE);
                        profileLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent prescriptionIntent = new Intent(getActivity(), PrescriptionActivity.class);
                                prescriptionIntent.putExtra("Profile", "Profile");
                                prescriptionIntent.putExtra("profile_id", Integer.toString((int) v.getTag()));
                                Log.v("Profile", Integer.toString((int) v.getTag()));
                                startActivity(prescriptionIntent);
                                NavigationUtility.animateLeft(getActivity());
                            }
                        });
                    case Profile:
                        /* Navigation to Profile screen*/
                }

            }
            return new ProfileHolder(view);

        }

        @Override
        public void onBindViewHolder(ProfileHolder holder, int position) {
            Log.v("Positionis", Integer.toString(position));
            final Profile profile = mProfileList.get(position);



            if (itemsPendingRemoval.contains(profile)) {
                // we need to show the "undo" state of the row
                holder.itemView.setBackgroundColor(Color.parseColor("#ff3232"));
                holder.normalLayout.setVisibility(View.GONE);
                holder.delLayout.setVisibility(View.VISIBLE);
                holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(profile);
                        pendingRunnables.remove(profile);
                        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(profile);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(mProfileList.indexOf(profile));
                    }
                });
            } else {
                // we need to show the "normal" state
                holder.itemView.setBackgroundColor(Color.WHITE);
                holder.delLayout.setVisibility(View.GONE);
                holder.normalLayout.setVisibility(View.VISIBLE);
                holder.bindProfile(profile);

            }
        }

        @Override
        public int getItemCount() {

            return mProfileList.size();
        }
        public void setUndoOn(boolean undoOn) {
            this.undoOn = undoOn;
        }

        public boolean isUndoOn() {
            return undoOn;
        }

        public void pendingRemoval(int position) {
            final Profile item = mProfileList.get(position);
            if (!itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.add(item);
                // this will redraw row in "undo" state
                notifyItemChanged(position);
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        IProfileManager profileManager = ProfileManager.get(getActivity());
                        profileManager.deleteProfile(item.getId());
                        // Update the UI now.
                        remove(mProfileList.indexOf(item));
                    }
                };
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
                pendingRunnables.put(item, pendingRemovalRunnable);
            }
        }

        public void remove(int position) {
            Profile item = mProfileList.get(position);
            if (itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.remove(item);
            }
            if (mProfileList.contains(item)) {
                mProfileList.remove(position);
                notifyItemRemoved(position);
            }
        }

        public boolean isPendingRemoval(int position) {
            Profile item = mProfileList.get(position);
            return itemsPendingRemoval.contains(item);
        }


    }
    @Override
    public void onResume() {
        super.onResume();
        mProfileAdapter.setProfileList(ProfileManager.get(getActivity()).getAllProfiles());
        mProfileAdapter.notifyDataSetChanged();
    }

}

