package iss.nus.edu.sg.pillpal.interfaces;

import java.util.ArrayList;
import java.util.List;

import iss.nus.edu.sg.pillpal.models.Prescription;

/**
 * Created by Samrat on 2/6/16.
 */
public interface IPrescriptionManager {

    /**
     * Method will add a prescription to the dB.
     * @param prescription The prescription that needs to be added to the dB.
     * @return The updated prescription that was added to the dB.
     */
    public Prescription addPrescription(Prescription prescription);

    /**
     * Method to delete the prescription that is present in the dB.
     * @param prescriptionId The id of the prescription that needs to be deleted.
     * @return True if the prescription was successfully deleted, else false.
     */
    public boolean deletePrescription(int prescriptionId);

    /**
     * Get all prescriptions that are attached to a particular profile.
     * @param profileId The profileId for which the prescriptions need to be searched.
     * @return The list of prescriptions. If there are no prescriptions available for the profile, an empty list is returned.
     */
    public List<Prescription> getPrescriptionsForProfile(int profileId);

    /**
     * Method to get a particular Prescription object based on its id.
     * @param prescriptionId The id of the prescription.
     * @return The Prescription object associated with the id, else null.
     */
    public Prescription getPrescription(int prescriptionId);

    /**
     * Method to update the validity of an existing prescription.
     * @param validity The updated validity of the prescription.
     * @param prescriptionId The prescription id that needs to be updated.
     * @return True if successful, else false.
     */
    public boolean updatePrescriptionValidity(int validity, int prescriptionId);

}
