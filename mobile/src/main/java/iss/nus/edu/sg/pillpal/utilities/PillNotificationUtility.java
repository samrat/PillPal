package iss.nus.edu.sg.pillpal.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;

import java.io.Serializable;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.broadcastReceiver.PillConsumeBroadCastReceiver;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.services.PillConsumeService;
import iss.nus.edu.sg.pillpal.services.PillExpireService;
import iss.nus.edu.sg.pillpal.services.PillOutOfStockService;
import iss.nus.edu.sg.pillpal.ui.activities.InventoryActivity;
import iss.nus.edu.sg.pillpal.ui.activities.MonitorActivity;
import iss.nus.edu.sg.pillpal.ui.activities.PrescriptionActivity;
import iss.nus.edu.sg.pillpal.ui.fragments.ViewPrescriptionFragment;

/**
 * Created by Premraj M on 19/5/2016.
 */
public class PillNotificationUtility {

	public static final String PILL_CONSUMED = "PillConsumed";
	public static final String PILL_ID = "PILL_ID";
	public static final String QUANTITY = "QUANTITY";

	/**
	 * Method to show the expiry notification.
	 * @param header The header of the notification
	 * @param detail The detail for the notification
	 * @param additionalInfo The additional information, if any, that needs to be displayed.
     */
	public static void notificationExpire(String header, String detail, String additionalInfo) {
		Intent resultIntent = new Intent(PillExpireService.context, InventoryActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(PillExpireService.context);
		stackBuilder.addParentStack(InventoryActivity.class);
		stackBuilder.addNextIntent(resultIntent);

		PillNotificationUtility.setNotification(header, detail, additionalInfo, stackBuilder, PillExpireService.context);

		/* For successful navigation */
		SharedPreferences settings = PillExpireService.context.getSharedPreferences("NOTIFICATION", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("NOTIFICATION", "EXPIRY");
		editor.commit();
	}

	/**
	 * Method to show the out of stock notification.
	 * @param header The header of the notification
	 * @param detail The detail for the notification
	 * @param additionalInfo The additional information, if any, that needs to be displayed.
	 */
	public static void notificationOutOfStock(String header, String detail, String additionalInfo) {
		Intent resultIntent = new Intent(PillOutOfStockService.context, InventoryActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(PillOutOfStockService.context);
		stackBuilder.addParentStack(InventoryActivity.class);
		stackBuilder.addNextIntent(resultIntent);

		PillNotificationUtility.setNotification(header, detail, additionalInfo, stackBuilder, PillOutOfStockService.context);

		/* For successful navigation */
		SharedPreferences settings = PillOutOfStockService.context.getSharedPreferences("NOTIFICATION", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("NOTIFICATION", "OUT_OF_STOCK");
		editor.commit();
	}

	/**
	 * Method to show notification for pill consumption.
	 * @param header The header of the notification
	 * @param detail The detail for the notification
	 */
	public static void notificationPillConsumption(String header, String detail, PrescriptionItem prescriptionItem) {

		Intent intentPillSkipped = new Intent(PillConsumeService.context, PillConsumeBroadCastReceiver.class);
		intentPillSkipped.putExtra(PILL_CONSUMED, false);
		PendingIntent pendingIntentPillSkipped = PendingIntent.getBroadcast(PillConsumeService.context, 0, intentPillSkipped, PendingIntent.FLAG_UPDATE_CURRENT);

		Intent intentPillConsumed = new Intent(PillConsumeService.context, PillConsumeBroadCastReceiver.class);
		intentPillConsumed.putExtra(PILL_CONSUMED, true);
		intentPillConsumed.putExtra(PILL_ID, prescriptionItem.getPill().getId());
		intentPillConsumed.putExtra(QUANTITY, Integer.parseInt(prescriptionItem.getDosage()));
		PendingIntent pendingIntentPillConsumed = PendingIntent.getBroadcast(PillConsumeService.context, 1, intentPillConsumed, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(PillConsumeService.context);
		mBuilder.setUsesChronometer(false)
				.setContentTitle(header)
				.setContentText(detail)
				.setAutoCancel(true)
				.setSmallIcon(R.mipmap.ic_notification)
				.addAction(R.drawable.check_circle,"Consumed",pendingIntentPillConsumed)
				.addAction(R.drawable.close_circle,"Later",pendingIntentPillSkipped);

		mBuilder.setPriority(Notification.PRIORITY_HIGH);
		mBuilder.setColor(Color.RED);
		mBuilder.setLights(Color.RED, 3000, 3000);
		mBuilder.setWhen(0);
		mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
		mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		NotificationManager mNotificationManager = (NotificationManager) PillConsumeService.context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify((int)PillConsumeService.serialVersionUID, mBuilder.build());
	}


	/**
	 * Method to display the notification to teh user that his heart rate has exceeded the normal level
	 * @param context The context.
	 * @param header The header of the notification
	 * @param detail The detail for the notification
	 * @param additionalInfo The additional information, if any, that needs to be displayed.
     */
	public static void notificationHighHeartRate(Context context, String header, String detail, String additionalInfo) {
		Intent resultIntent = new Intent(context, MonitorActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(InventoryActivity.class);
		stackBuilder.addNextIntent(resultIntent);

		PillNotificationUtility.setNotification(header, detail, additionalInfo, stackBuilder, context);
	}

	/**
	 * Method to create notifications.
	 * @param header The header of the notification
	 * @param detail The detail for the notification
	 * @param additionalInfo The additional information, if any, that needs to be displayed.
	 * @param stackBuilder The stack builder object with the information about the navigation attahced to the notification.
     */
	private static void setNotification(String header, String detail, String additionalInfo, TaskStackBuilder stackBuilder, Context context) {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
		mBuilder.setUsesChronometer(false)
				.setContentTitle(header)
				.setContentText(detail)
				.setSubText(additionalInfo)
				.setAutoCancel(true)
				.setSmallIcon(R.mipmap.ic_notification);

		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

		mBuilder.setPriority(Notification.PRIORITY_HIGH);
		mBuilder.setContentIntent(resultPendingIntent);
		mBuilder.setColor(Color.RED);
		mBuilder.setLights(Color.RED, 3000, 3000);
		mBuilder.setWhen(0);
		mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
		mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify((int)PillOutOfStockService.serialVersionUID, mBuilder.build());
	}
}
