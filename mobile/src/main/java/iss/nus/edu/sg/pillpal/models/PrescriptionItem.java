package iss.nus.edu.sg.pillpal.models;

public class PrescriptionItem {
    /**************************************************/
    // Instance Variables
    /**************************************************/
    /**
     * The id of the prescription item
     */
    private int mId;
    /**
     * The pill attached to this item
     */
    private Pill mPill;
    /**
     * The routine of this item
     */
    private String mRoutine;
    /**
     * The dosage of this item.
     */
    private String mDosage;
    /**
     * item instructions
     */
    private String mInstructions;

    /**************************************************/
    // Constructors
    /**************************************************/

    public PrescriptionItem(){ }


    public PrescriptionItem(int id, Pill pill, String routine, String dosage,String instructions) {
        mId = id;
        mPill = pill;
        mRoutine = routine;
        mDosage = dosage;
        mInstructions = instructions;
    }

    /**************************************************/
    // Getters & Setters
    /**************************************************/

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Pill getPill() {
        return mPill;
    }

    public void setPill(Pill pill) {
        mPill = pill;
    }

    public String getRoutine() {
        return mRoutine;
    }

    public void setRoutine(String routine) {
        mRoutine = routine;
    }

    public String getDosage() {
        return mDosage;
    }

    public void setDosage(String dosage) {
        mDosage = dosage;
    }

    public String getInstructions() {
        return mInstructions;
    }

    public void setInstructions(String mInstructions) {
        this.mInstructions = mInstructions;
    }
}
