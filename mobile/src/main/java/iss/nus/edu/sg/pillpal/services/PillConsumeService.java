package iss.nus.edu.sg.pillpal.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import iss.nus.edu.sg.pillpal.models.ScheduledTask;
import iss.nus.edu.sg.pillpal.scheduler.Scheduler;
import iss.nus.edu.sg.pillpal.scheduler.tasks.PillConsumeTask;

/**
 * Created by Premraj M on 2/9/2016.
 */
public class PillConsumeService extends Service {

    /****************************************************/
    // Constants
    /****************************************************/

    public static final long serialVersionUID = 1L;
    public static PillConsumeService context = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        context = PillConsumeService.this;
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Scheduler.stop(serialVersionUID);
        super.onDestroy();
    }

    /****************************************************/
    // Defining the intervals of the notifications
    /****************************************************/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ScheduledTask scheduledTask = new ScheduledTask();
        scheduledTask.setClassName(PillConsumeTask.class.getName());
        scheduledTask.setId(serialVersionUID);
        SharedPreferences settings = getSharedPreferences("TIME", 0);
        scheduledTask.setDailyAt(settings.getString("PRH", "9")+":"+settings.getString("PRRM", "00"));
        scheduledTask.setTaskName(PillConsumeTask.class.getName());
//        scheduledTask.setInterval(60000l);
//        scheduledTask.setTaskName(PillConsumeTask.class.getName());
        // add task to scheduler and start it based on the interval value or daily at
        Scheduler.addTask(scheduledTask);
        return super.onStartCommand(intent, flags, startId);
    }

}
