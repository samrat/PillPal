package iss.nus.edu.sg.pillpal.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.PrescriptionManager;
import iss.nus.edu.sg.pillpal.interfaces.IPrescriptionManager;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;
import iss.nus.edu.sg.pillpal.utilities.DefaultDividerItemDecoration;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;

/**
 * Created by Koushik
 */
public class ViewPrescriptionFragment extends Fragment {

    /*
            Instance variables
     */
    /*
           Prescription Id selected from the list of Prescriptions
     */
    int mPrescriptionId;
    /*
        Prescription medicine list item Recycler view
     */
    @Bind(R.id.prescription_medicine_recycler_view)
    RecyclerView mPrescriptionMedicineList;


    /*
            Prescription Manager
     */
    IPrescriptionManager mPrescriptionManager;
    Prescription prescription;
    /*
            List of EditTexts for View Prescription -starts
     */
    EditText mClinicName;
    EditText mDoctorName;
    EditText mPrescriptionDate;
    EditText mPrescriptionExpiry;
    /*
                    List of EditTexts for View Prescription -ends

     */
    /*
            PrescriptionItemAdapter - RecylerView Adapter
     */
    private PrescriptionItemAdapter mPrescriptionItemAdapter;
    /*
        Save Button
     */

    public ViewPrescriptionFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_prescription, container, false);
        mClinicName = (EditText) view.findViewById(R.id.clinic_name);
        mDoctorName = (EditText) view.findViewById(R.id.doctor_name);
        mPrescriptionDate = (EditText) view.findViewById(R.id.prescription_date);
        ButterKnife.bind(this, view);
        mPrescriptionManager = PrescriptionManager.get(getActivity());
        mPrescriptionMedicineList.setLayoutManager(new LinearLayoutManager(getActivity()));

        mPrescriptionId = Integer.parseInt(getActivity().getIntent().getStringExtra("PrescriptionId"));
        prescription = mPrescriptionManager.getPrescription(mPrescriptionId);
        mPrescriptionExpiry = (EditText) view.findViewById(R.id.prescription_expiry_date);
        List<PrescriptionItem> prescriptionItems = prescription.getPrescriptionItems();



        if(prescription!=null){
            mClinicName.setText(prescription.getClinicName());
            mDoctorName.setText(prescription.getDoctorName());
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            Date prescriptiondate = prescription.getPrescriptionDate();
            Date prescriptionExpiryDate = prescription.getPrescriptionExpiryDate();
            try {
                String prescriptionDateString = df.format(prescriptiondate);
                String prescriptionExpiryDateString = df.format(prescriptionExpiryDate);
            mPrescriptionDate.setText(prescriptionDateString);
            mPrescriptionExpiry.setText(prescriptionExpiryDateString);
            updatePrescriptionItems( prescriptionItems);

        }catch (Exception e){
                e.printStackTrace();
            }



    }
        return view;

    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }
    /*
            ViewPrescriptionFragment - Private methods
     */

    /**
     *
     * @param prescriptionItems Pill Details such as Name,Dosage and Routine are displayed in Recycler view
     */
    private void updatePrescriptionItems(List<PrescriptionItem> prescriptionItems){
        mPrescriptionItemAdapter = new PrescriptionItemAdapter(prescriptionItems);
        mPrescriptionMedicineList.setAdapter(mPrescriptionItemAdapter);
        mPrescriptionMedicineList.addItemDecoration(new DefaultDividerItemDecoration(getActivity()));


    }

    /*
            Presription Items holder
     */
    private class PrescriptionItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        /*
        PrescriptionItemHolder - Instance Variables
         */
        private TextView mPillName;
        private TextView mDosage;
        private TextView mRoutine;
        /*
            PrescriptionItemHolder - Public Constructors
         */


       public  PrescriptionItemHolder(View itemView){
            super(itemView);
            mPillName = (TextView) itemView.findViewById(R.id.pill_name);
            mDosage = (TextView) itemView.findViewById(R.id.pill_dosage);
            mRoutine = (TextView)itemView.findViewById(R.id.pill_routine);

        }

        /*
            PrescriptionItemHolder - Public Methods
         */
        /*
            Bind the Prescription items to the holder view
         */
        public void bindProfile(iss.nus.edu.sg.pillpal.models.PrescriptionItem prescriptionItem ){
            mPillName.setText(prescriptionItem.getPill().getName());
            mDosage.setText(prescriptionItem.getDosage());
            mRoutine.setText(prescriptionItem.getRoutine());
        }
            /*
                PrescriptionItemHolder abstract methods
             */
        @Override
        public  void onClick(View v){
            /* Abstract method */
        }


    }
    /*
            PrescriptionItems - Adapter
     */
    private class PrescriptionItemAdapter extends RecyclerView.Adapter<PrescriptionItemHolder>{

        /*
            PrescriptionAdapter Instance variables
         */
        private List<PrescriptionItem> mPrescriptionItems;
        /*

            PrescriptionItemAdapter - public methods
         */
        public void setmPrescriptionItems(List<PrescriptionItem> prescriptionItems){
            mPrescriptionItems = prescriptionItems;
        }
        /*
                PrescriptionItemAdapter -Public Constructors -
         */

        public PrescriptionItemAdapter(List<PrescriptionItem> prescriptionItems) {
            mPrescriptionItems = prescriptionItems;
        }

        /*
            PrescriptionItemAdapter Abstract method Implementations
         */
        public PrescriptionItemHolder onCreateViewHolder(ViewGroup parent,int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_prescription_medicines,parent,false);
            return new PrescriptionItemHolder(view);
        }
        @Override
        public void onBindViewHolder(PrescriptionItemHolder holder,int position){
            PrescriptionItem prescriptionItem = mPrescriptionItems.get(position);
            holder.bindProfile(prescriptionItem);
        }

        @Override
        public int getItemCount(){
            return mPrescriptionItems.size();
        }


    }
}
