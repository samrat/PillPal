package iss.nus.edu.sg.pillpal.scheduler.tasks;

import java.util.TimerTask;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public abstract class CommonTask extends TimerTask {
	
	private Long taskId;
	private String taskName;
	
	/**
	 * Save task success history
	 */
	public void saveSucess() {
		
	}
	

	/**
	 * Save task error history
	 */
	public void saveError(String errorLog, Exception e) {
		
	}
	
	/**
	 * Any extended class must implement this. The method is invoked when the task run
	 * @throws Exception
	 */
	public abstract void runTask() throws Exception;
	
	/**
	 * Run the task
	 */
	public void run() {
		try {
			runTask();
			saveSucess();
		} catch (Exception e) {
			saveError(taskName +"---"+ e.getMessage(), e);
		}
	}
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
}