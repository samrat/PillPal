package iss.nus.edu.sg.pillpal.scheduler.tasks;

import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.services.PillOutOfStockService;
import iss.nus.edu.sg.pillpal.utilities.PillNotificationUtility;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class PillOutOfStockTask extends CommonTask {

	@Override
	public void runTask() throws Exception {
		InventoryManager manager = InventoryManager.get(PillOutOfStockService.context);
		boolean showNotification = false;
		String message = "";
		for (Pill pill: manager.getOutOfStock()) {
			message = "Some pills will be out of stock soon. Please check!";
			showNotification = true;
		}
		if (showNotification) {
			PillNotificationUtility.notificationOutOfStock("Pills out of stock", message, "Click here to see more.");
		}
	}
}
