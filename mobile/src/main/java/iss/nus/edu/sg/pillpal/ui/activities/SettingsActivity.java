package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.SettingsFragment;

public class SettingsActivity extends BaseSingleFragmentActivity {

	protected Fragment createFragment() {
		return new SettingsFragment();
	}
}
