package iss.nus.edu.sg.pillpal.data;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import iss.nus.edu.sg.pillpal.data.PillPalDbSchema.PrescriptionItemTable;
import iss.nus.edu.sg.pillpal.data.PillPalDbSchema.PrescriptionTable;
import iss.nus.edu.sg.pillpal.interfaces.IPrescriptionManager;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;

/**
 * Created by Samrat on 2/6/16.
 */
public class PrescriptionManager implements IPrescriptionManager {
    /**************************************************/
    // Constants
    /**************************************************/
    private static final String TAG = "PrescriptionManager";

    /**************************************************/
    // Instance Variables
    /**************************************************/
    private Context mContext;
    private PillPalBaseHelper mPillPalBaseHelper;

    private static PrescriptionManager sPrescriptionManager;

    /**************************************************/
    // Constructor

    /**************************************************/
    private PrescriptionManager(Context context) {
        mContext = context.getApplicationContext();
        mPillPalBaseHelper = new PillPalBaseHelper(mContext);
        try {
            mPillPalBaseHelper.createDB();
        } catch (Exception e) {
            Log.d(TAG, "DB not created");
        }
    }

    /**************************************************/
    // Public Methods
    /**************************************************/

    /**
     * Returns the singleton instance of the Prescription Manager
     *
     * @param context The current context of the caller
     * @return The instance of the Prescription Manager
     */
    public static PrescriptionManager get(Context context) {
        if (sPrescriptionManager == null) {
            sPrescriptionManager = new PrescriptionManager(context);
        }
        return sPrescriptionManager;
    }
    /**
     * Method to update the validity of an existing prescription.
     * @param validity The updated validity of the prescription.
     * @param prescriptionId The prescription id that needs to be updated.
     * @return True if successful, else false.
     */
    public boolean updatePrescriptionValidity(int validity, int prescriptionId) {
        // Cursor cursor = null;
        try {
            mPillPalBaseHelper.openDB();
            String updateQuery = getPrescriptionValidityUpdateQuery(validity, prescriptionId);
            // Fire the insert
            mPillPalBaseHelper.getWritableDatabase().execSQL(updateQuery);


//            if(cursor.getCount() > 0) {
            return true;
//            }
        } catch (Exception e) {
            Log.d(TAG, "Exception while updatePrescriptionValidity from dB");
            e.printStackTrace();
        } finally {
            mPillPalBaseHelper.close();
        }
        return false;
    }

    /*****************************************************/
    // IPrescriptionManager implementation
    /*****************************************************/
    /**
     * Method will add a prescription to the dB.
     *
     * @param prescription The prescription that needs to be added to the dB.
     * @return The updated prescription that was added to the dB.
     */
    public Prescription addPrescription(Prescription prescription) {

        Prescription createdPrescription = null;

        // First check if the prescription is valid or not.
        // It also needs to have at least one prescription item attached to it.
        if(validatePrescriptionBeforeAdding(prescription)) {

            Cursor cursor = null;

            try {
                // Open the dB
                mPillPalBaseHelper.openDB();
                mPillPalBaseHelper.getWritableDatabase().beginTransaction();

                // Start adding in the Prescription Table

                // The profile ID
                int prescriptionId = 0;
                // Insert Query
                String insertQuery = getPrescriptionInsertQuery(prescription);
                // Fire the insert
                mPillPalBaseHelper.getWritableDatabase().execSQL(insertQuery);
                // Time to retrieve the last row
                cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery("SELECT last_insert_rowid();", null);
                try {
                    while (cursor.moveToNext()) {
                        prescriptionId = cursor.getInt(0);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception while getting last updated profileId");
                    e.printStackTrace();
                }

                // Now start putting it in the prescription item
                for (PrescriptionItem item : prescription.getPrescriptionItems()) {
                    String prescriptionItemInsertQuery = getItemInsertQuery(item, prescriptionId);
                    // Fire the insert
                    mPillPalBaseHelper.getWritableDatabase().execSQL(prescriptionItemInsertQuery);
                }

                // Set the complete transaction as success
                mPillPalBaseHelper.getWritableDatabase().setTransactionSuccessful();
                mPillPalBaseHelper.getWritableDatabase().endTransaction();
                // Finally retrieve the item that was pushed.
                return getPrescription(prescriptionId);
            } catch (Exception e) {
                Log.d(TAG, "Exception while addProfile");
                e.printStackTrace();
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                mPillPalBaseHelper.close();
            }
        }
        return createdPrescription;
    }

    /**
     * Method to get the query for updating the validity of a prescription.
     * @param validity The udpated validity of the prescription.
     * @param prescriptionId The prescription that needs to be updated.
     * @return The updated UPDATE query.
     */
    private String getPrescriptionValidityUpdateQuery(int validity, int prescriptionId) {
        String query = "UPDATE " + PrescriptionTable.NAME + " SET " +
                PrescriptionTable.Cols.EXPIRY_DATE + " = " + validity +
                " WHERE " + PrescriptionTable.Cols.ID + " = " +
                + prescriptionId + ";";
        return query;
    }

    /**

     /**
     * Method to get a prescription using the prescription Id.
     *
     * @param prescriptionId The prescription Id depending on which the prescription will be returned.
     * @return The prescription associated with the provided Id. If not found, then null is returned.
     */
    public Prescription getPrescription(int prescriptionId) {
        Cursor cursor = null;
        Prescription prescription = new Prescription();
        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + PrescriptionTable.NAME + " WHERE " + PrescriptionTable.Cols.ID + " = " + prescriptionId;
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            try {
                while (cursor.moveToNext()) {
                    prescription = getPrescription(cursor);
                    int profileId = cursor.getInt(cursor.getColumnIndex(PrescriptionTable.Cols.PROFILE_ID));
                    prescription.setProfile(ProfileManager.get(mContext).getProfile(profileId));
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getProfile");
                e.printStackTrace();
                // Make sure that the state is returned to null.
                prescription = null;
            }

            // Now get the prescription items attached to the prescription
            String itemQuery = getItemsForPrescriptionIdQuery(prescription);
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(itemQuery, null);

            try {
                while (cursor.moveToNext()) {
                    PrescriptionItem prescriptionItem = getPrescriptionItem(cursor);
                    int pillId = cursor.getInt(cursor.getColumnIndex(PrescriptionItemTable.Cols.PILL_ID));
                    prescriptionItem.setPill(InventoryManager.get(mContext).getPill(pillId));
                    prescription.getPrescriptionItems().add(prescriptionItem);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getProfile");
                e.printStackTrace();
                // Make sure that the state is returned to null.
                prescription = null;
            }

            return prescription;
        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getProfile");
            e.printStackTrace();
        } finally {
            cursor.close();
            mPillPalBaseHelper.close();
        }
        // Return null
        return prescription;
    }

    /**
     * Get all prescriptions that are attached to a particular profile.
     * @param profileId The profileId for which the prescriptions need to be searched.
     * @return The list of prescriptions. If there are no prescriptions available for the profile, an empty list is returned.
     */
    public List<Prescription> getPrescriptionsForProfile(int profileId) {
        List<Prescription> prescriptionList= new ArrayList<>();
        Cursor cursor = null;
        try {
            mPillPalBaseHelper.openDB();
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(getPrescriptionForProfileIdQuery(profileId), null);
            try {
                while (cursor.moveToNext()) {
                    Prescription prescription = getPrescription(cursor);
                    prescription.setProfile(ProfileManager.get(mContext).getProfile(profileId));
                    prescriptionList.add(prescription);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getPrescriptionsForProfile");
                e.printStackTrace();
            }

            // Loop the list of prescriptions now & get the prescription items for each
            for(Prescription prescription : prescriptionList ) {
                cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(getItemsForPrescriptionIdQuery(prescription),null);
                try {
                    while (cursor.moveToNext()) {
                        PrescriptionItem prescriptionItem = getPrescriptionItem(cursor);
                        int pillId = cursor.getInt(cursor.getColumnIndex(PrescriptionTable.Cols.ID));
                        prescriptionItem.setPill(InventoryManager.get(mContext).getPill(pillId));
                        prescription.getPrescriptionItems().add(prescriptionItem);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception while fetching getPrescriptionsForProfile");
                    e.printStackTrace();
                }
            }
        }catch (Exception e) {
            Log.d(TAG, "Exception while fetching getPrescriptionsForProfile");
            e.printStackTrace();
        }finally {
            if(cursor != null) {
                cursor.close();
            }
            mPillPalBaseHelper.close();
        }

        return prescriptionList;
    }

    /**
     * Method to delete the prescription that is present in the dB.
     *
     * @param prescriptionId The id of the prescription that needs to be deleted.
     * @return True if the prescription was successfully deleted, else false.
     */
    public boolean deletePrescription(int prescriptionId) {
        boolean blReturnValue = false;
        try {
            mPillPalBaseHelper.openDB();
            mPillPalBaseHelper.getWritableDatabase().beginTransaction();
            mPillPalBaseHelper.getWritableDatabase().delete(PrescriptionItemTable.NAME, PrescriptionItemTable.Cols.PRESCRIPTION_ID + " = " + prescriptionId, null);
            mPillPalBaseHelper.getWritableDatabase().delete(PrescriptionTable.NAME, PrescriptionTable.Cols.ID + " = " + prescriptionId, null);
            mPillPalBaseHelper.getWritableDatabase().setTransactionSuccessful();
            blReturnValue = true;
        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getPill from dB");
            e.printStackTrace();
        } finally {
            mPillPalBaseHelper.getWritableDatabase().endTransaction();
            mPillPalBaseHelper.close();
        }

        return blReturnValue;
    }
    /*****************************************************/
    // Private Methods
    /*****************************************************/

    /**
     * Method to create a query for getting a prescription for a particular profile.
     * @param profileId The profile id for which eh prescription query needs to be created.
     * @return The get query.
     */
    private String getPrescriptionForProfileIdQuery(int profileId) {
        String query = "SELECT * FROM " + PrescriptionTable.NAME + " WHERE " +
                PrescriptionTable.Cols.PROFILE_ID + " = " +
                + profileId + ";";
        return query;
    }

    /**
     * Method to create the insert query.
     *
     * @param prescription The prescription for which the query needs to be created.
     * @return The updated INSERT query.
     */
    private String getPrescriptionInsertQuery(Prescription prescription) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String insertQuery = "INSERT INTO " + PrescriptionTable.NAME +
                " ('" + PrescriptionTable.Cols.PROFILE_ID + "'," +
                "'" + PrescriptionTable.Cols.EXPIRY_DATE + "'," +
                "'" + PrescriptionTable.Cols.DOCTOR_NAME + "'," +
                "'" + PrescriptionTable.Cols.CLINIC_NAME + "'," +
                "'" + PrescriptionTable.Cols.PRESCRIPTION_DATE + "')" +
                " VALUES(" +
                prescription.getProfile().getId() + "," +
                "'" +formatter.format(prescription.getPrescriptionExpiryDate()) + "'," +
                "'" + prescription.getDoctorName() + "'," +
                "'" + prescription.getClinicName() + "'," +
                "'" + formatter.format(prescription.getPrescriptionDate()) + "'" + ");";
        return insertQuery;
    }

    /**
     * Method to get the insert query for inserting an prescription item in dB
     * @param item The item that needs to be inserted
     * @param prescriptionId The prescriptionId that this item is attached to.
     * @return The query.
     */
    private String getItemInsertQuery(PrescriptionItem item, int prescriptionId) {
        String insertQuery = "INSERT INTO " + PrescriptionItemTable.NAME +
                " ('" + PrescriptionItemTable.Cols.PILL_ID + "'," +
                "'" + PrescriptionItemTable.Cols.DOSAGE + "'," +
                "'" + PrescriptionItemTable.Cols.ROUTINE + "'," +
                "'" + PrescriptionItemTable.Cols.INSTRUCTIONS + "'," +
                "'" + PrescriptionItemTable.Cols.PRESCRIPTION_ID + "')" +
                " VALUES(" +
                item.getPill().getId() + "," +
                "'" + item.getDosage() + "'," +
                "'" + item.getRoutine() + "'," +
                "'" + item.getInstructions() + "'," +

                prescriptionId + ");";
        return insertQuery;
    }

    /**
     * Method to create a select query with where clause for prescription Id.
     *
     * @param prescription The prescription for which the query needs to be created.
     * @return The updated SELECT query.
     */
    private String getItemsForPrescriptionIdQuery(Prescription prescription) {
        String query = "SELECT * FROM " + PrescriptionItemTable.NAME + " WHERE " +
                PrescriptionItemTable.Cols.PRESCRIPTION_ID + " = " +
                +prescription.getId() + ";";
        return query;
    }

    /**
     * Gets a particular prescription item from the cursor. This does not set the pill object attached to it.
     * @param cursor The cursor from which the prescription item needs to be returned
     * @return The prescription item object having all the retrieved values from the cursor.
     * If an attribute is not found, then it remains null.
     */
    private PrescriptionItem getPrescriptionItem(Cursor cursor) {
        //TODO
        PrescriptionItem prescriptionItem = new PrescriptionItem();
        prescriptionItem.setId(cursor.getInt(cursor.getColumnIndex(PrescriptionItemTable.Cols.ID)));
        prescriptionItem.setDosage(cursor.getString(cursor.getColumnIndex(PrescriptionItemTable.Cols.DOSAGE)));
        prescriptionItem.setRoutine(cursor.getString(cursor.getColumnIndex(PrescriptionItemTable.Cols.ROUTINE)));
        prescriptionItem.setInstructions(cursor.getString(cursor.getColumnIndex(PrescriptionItemTable.Cols.INSTRUCTIONS)));
        return prescriptionItem;
    }

    /**
     * Method to retrieve the Prescription from the cursor object.
     * @param cursor The dB cursor from which the object needs to be read.
     * @return The presccription object. This will not have the profile & prescription items.
     */
    private Prescription getPrescription(Cursor cursor) {

        Prescription prescription = new Prescription();
        prescription.setId(cursor.getInt(cursor.getColumnIndex(PrescriptionTable.Cols.ID)));
        prescription.setDoctorName(cursor.getString(cursor.getColumnIndex(PrescriptionTable.Cols.DOCTOR_NAME)));
        prescription.setClinicName(cursor.getString(cursor.getColumnIndex(PrescriptionTable.Cols.CLINIC_NAME)));
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String prescriptionDate = (cursor.getString(cursor.getColumnIndex(PrescriptionTable.Cols.PRESCRIPTION_DATE)));
            prescription.setPrescriptionDate(dateFormat.parse(prescriptionDate));
            String prescriptionExpiryDate = (cursor.getString(cursor.getColumnIndex(PrescriptionTable.Cols.EXPIRY_DATE)));
            Log.v("prescriptionExpiryDate",prescriptionExpiryDate);
            prescription.setPrescriptionExpiryDate(dateFormat.parse(prescriptionExpiryDate));

        }catch (Exception e) {
            Log.d(TAG,"Exception due to date parsing from db");
            e.printStackTrace();
        }
        return prescription;
    }

    /**
     * Checks if all the required parameters of the prescription are present before adding.
     * @param prescription The prescription item that needs to be added.
     * @return True if the item valid, else false.
     */
    private boolean validatePrescriptionBeforeAdding(Prescription prescription) {

        if(prescription != null &&
                prescription.getProfile() != null &&
                prescription.getPrescriptionDate() != null &&
                prescription.getDoctorName() != null &&
                prescription.getPrescriptionItems().size() > 0) {

            // Now also check that each item is attached to an actual pill
            for(PrescriptionItem prescriptionItem : prescription.getPrescriptionItems()) {
                if(InventoryManager.get(mContext).getPill(prescriptionItem.getPill().getId()) == null) {
                    return false;
                }
            }
            return  true;
        }
        return false;
    }
}
