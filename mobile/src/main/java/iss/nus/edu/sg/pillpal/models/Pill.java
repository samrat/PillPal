package iss.nus.edu.sg.pillpal.models;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Pill {

    /**************************************************/
    // Instance Variables
    /**************************************************/

    /**
     * The identifier of the pill.
     */
    private int mId;

    /**
     * The name of the pill.
     */
    private String mName;

    /**
     * Details of the pill
     */
    private String mDetails;

    /**
     * Quantity of pill available. (Takes into account the different batches)
     */
    private int mQuantity;

    /**
     * Unit of pill (tab or ml)
     */
    private String mUnit;

    /**
     * The threshold quantity of the pill, after which the user will be notified.
     */
    private int mThresholdQuantity;

    /**
     * The number of days threshold for expiry
     */
    private int mNumberOfDaysThresholdForExpiry;

    /**
     * The expiry date of the batch that is going to expire soonest.
     * This is calculated based on the batches associated with the pill.
     */
    private Date mExpiryDate;

    /**
     * The batches associated with the pill
     */
    private ArrayList<Batch> mBatches;

    /**
     * Whether any batch of the Pill is expired.
     * Calculated based on the batches associated with the pill.
     */
    private boolean mIsExpired;

    /**
     * Whether the pill is out of stock.
     * Calculated based on the batches associated with the pill.
     */
    private boolean mIsOutOfStock;

    /**************************************************/
    // Constructors
    /**************************************************/
    /**
     * Default Constructor
     */
    public Pill() {
        mBatches = new ArrayList<>();
    }

    /**
     *
     * @param m_id
     * @param m_name
     * @param m_details
     * @param m_quantity
     * @param m_unit
     * @param m_thresholdQuantity
     * @param m_thresholdDate
     */
    public Pill(int m_id, String m_name, String m_details, int m_quantity, String m_unit, int m_thresholdQuantity, Date m_thresholdDate) {
        mBatches = new ArrayList<>();
        this.mId = m_id;
        this.mName = m_name;
        this.mDetails = m_details;
        this.mQuantity = m_quantity;
        this.mUnit = m_unit;
        this.mThresholdQuantity = m_thresholdQuantity;
        //this.mThresholdDate = m_thresholdDate;
    }

    /**************************************************/
    // Getters & Setters
    /**************************************************/

    // ID
    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    // Name
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    // Details
    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    // Quantity
    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    // Unit
    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    // Threshold Quantity
    public int getThresholdQuantity() {
        return mThresholdQuantity;
    }

    public void setThresholdQuantity(int thresholdQuantity) {
        mThresholdQuantity = thresholdQuantity;
    }

    // Latest expiry date
    public Date getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        mExpiryDate = expiryDate;
    }

    // Number of days for threshold
    public int getNumberOfDaysThresholdForExpiry() {
        return mNumberOfDaysThresholdForExpiry;
    }

    public void setNumberOfDaysThresholdForExpiry(int numberOfDaysThresholdForExpiry) {
        mNumberOfDaysThresholdForExpiry = numberOfDaysThresholdForExpiry;
    }

    // Expired or not
    public void setExpired(boolean expired) {
        mIsExpired = expired;
    }

    public boolean isExpired() {
        return mIsExpired;
    }

    // Out of stock
    public void setOutOfStock(boolean outOfStock) {
        mIsOutOfStock = outOfStock;
    }

    public boolean isOutOfStock() {
        return mIsOutOfStock;
    }

    // List of batches
    public ArrayList<Batch> getBatches() {
        return mBatches;
    }

    public void setBatches(ArrayList<Batch> batches) {
        mBatches = batches;
    }
}