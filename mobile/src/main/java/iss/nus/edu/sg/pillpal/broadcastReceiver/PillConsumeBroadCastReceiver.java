package iss.nus.edu.sg.pillpal.broadcastReceiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.services.PillConsumeService;
import iss.nus.edu.sg.pillpal.utilities.PillNotificationUtility;

/**
 * Created by Samrat on 21/9/16.
 */

public class PillConsumeBroadCastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Make a toast to the user.
        if (intent != null && intent.hasExtra(PillNotificationUtility.PILL_CONSUMED)) {
            Boolean isConsumed = intent.getBooleanExtra(PillNotificationUtility.PILL_CONSUMED, false);
            int pillId = intent.getIntExtra(PillNotificationUtility.PILL_ID, 0);
            int quantity = intent.getIntExtra(PillNotificationUtility.QUANTITY, 0);
            Toast.makeText(context, "" + pillId + " Quantity = " + quantity, Toast.LENGTH_LONG).show();
            if(isConsumed) {
                // Remove from inventory
                InventoryManager.get(PillConsumeService.context).updatePillForConsumption(pillId, quantity);
            }else {
                // Show an alert
                Toast.makeText(context, "I'll remind you again in sometime.", Toast.LENGTH_LONG).show();
            }
            // Make sure to dismiss that the notification is removed once clicked.
            NotificationManager mNotificationManager = (NotificationManager) PillConsumeService.context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel((int) PillConsumeService.serialVersionUID);
        }

    }
}
