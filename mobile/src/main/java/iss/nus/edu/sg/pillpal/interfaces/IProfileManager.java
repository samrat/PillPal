package iss.nus.edu.sg.pillpal.interfaces;

import java.util.List;

import iss.nus.edu.sg.pillpal.models.Profile;

/**
 * Created by Samrat on 27/5/16.
 */
public interface IProfileManager {

    /**
     * Method will add a profile to the dB.
     * @return The updated profile that was added to the dB.
     */
    public Profile addProfile(Profile profile);

    /**
     * Method to get all the profiles present in the dB.
     * @return List of all profile objects that are present in dB.
     *         If there are no objects, then empty arraylist is returned.
     */
    public List<Profile> getAllProfiles();

    /**
     * Method to get a profile using the profile Id.
     * @param profileId The profile Id depending on which the profile will be returned.
     * @return The profile associated with the provided Id. If not found, then null is returned.
     */
    public Profile getProfile(int profileId);

    /**
     * Method to remove a profile from the dB.
     * @param profileId The associated profile Id that needs to be removed.
     * @return true if the profile was successfully removed , else false.
     */
    public boolean deleteProfile(int profileId);
}
