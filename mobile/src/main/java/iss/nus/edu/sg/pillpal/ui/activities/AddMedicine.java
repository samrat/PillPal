package iss.nus.edu.sg.pillpal.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.ui.fragments.AddNewBatchFragment;
import iss.nus.edu.sg.pillpal.ui.fragments.AddNewMedicineFragment;


public class AddMedicine extends BaseSingleFragmentActivity {

    /****************************************************/
    // Constants
    /****************************************************/
    public static final String PICTURE_PATH = "picturePath";

    /****************************************************/
    // Abstract Method Implementation
    /****************************************************/
    protected Fragment createFragment() {
        Bundle args = getIntent().getExtras();
        if (args != null && getIntent().getExtras().containsKey("PILL_ID")) {
            return new AddNewBatchFragment();
        } else {
            return new AddNewMedicineFragment();
        }
    }

}
