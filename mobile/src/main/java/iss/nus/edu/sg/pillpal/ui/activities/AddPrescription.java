package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.CreatePrescriptionFragment;

public class AddPrescription extends BaseSingleFragmentActivity {

    protected Fragment createFragment() {
        return new CreatePrescriptionFragment();
    }
}
