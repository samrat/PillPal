package iss.nus.edu.sg.pillpal.scheduler;

import iss.nus.edu.sg.pillpal.models.ScheduledTask;
import iss.nus.edu.sg.pillpal.scheduler.tasks.CommonTask;
import iss.nus.edu.sg.pillpal.utilities.StringUtility;

import java.util.Timer;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class TaskManager {

	private static long ONE_DAY = 24*60*60*1000;
	 
	private Timer timer;
	private ScheduledTask scheduledTask = null;
	private CommonTask task = null;
	
	public TaskManager(ScheduledTask scheduledTask) {
		this.scheduledTask = scheduledTask;
		schedule();
	}
	
	 /**
	  * 
	  * @param sharedTask
	  */
	 public synchronized void schedule() {
		 try {
			 try { //cancel the current task
				 if (timer!=null) {
					 timer.cancel();
				 }
			 } catch (Exception e) {
				//nothing
			 }
			 timer = new Timer();
			 String className = scheduledTask.getClassName();
			 Class classz = Class.forName(className);
			 task = (CommonTask)classz.newInstance();
			 task.setTaskId(scheduledTask.getId());
			 task.setTaskName(scheduledTask.getTaskName());
			 
			 if (scheduledTask.getInterval()>0) {
				 timer.scheduleAtFixedRate(task, 10000, scheduledTask.getInterval().longValue());
			 } else {
				 timer.scheduleAtFixedRate(task, StringUtility.converStringTimeToDate(scheduledTask.getDailyAt()), ONE_DAY); // daily
			 }
		 } catch (Exception e) {
			
		}
	}
	
	 /**
	  * Cancel the task
	  */
	 public void cancel() {
		 try {
			 timer.cancel();
		 } catch (Exception e) {
			e.printStackTrace();
		}
	 }
	 
	 public Long getTaskId() {
		 if (scheduledTask == null) {
			 return Long.valueOf(-1);
		 }
		 return scheduledTask.getId();
	 }
}