package iss.nus.edu.sg.pillpal.adapter;

import android.app.Activity;
import android.content.Context;
import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.interfaces.IInventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.ui.fragments.InventoryFragment;

/**
 * @author Bavithra Thangaraj on 8/13/2016.
 */
public class BatchAdapter extends BaseAdapter {


    private ArrayList<Batch> listData;
    private LayoutInflater layoutInflater;
    private Activity mActivity;

    public BatchAdapter(Activity activity, Context context, ArrayList<Batch> listData) {
        this.layoutInflater = LayoutInflater.from(context);
        this.listData = listData;
        this.mActivity = activity;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void swapItems(ArrayList<Batch> items) {
        this.listData.clear();
        this.listData = items;
//        this.listData.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_batch, null);
            holder = new ViewHolder();
//            holder.deleteBatchButton = (ImageView) convertView.findViewById(R.id.delete_batch_button);
            holder.deleteBatchButton.setTag(position);
            final Batch batch = (Batch) getItem(position);

            holder.deleteBatchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = (int)view.getTag();
                    listData.remove(index);
                    InventoryManager.get(mActivity).deleteBatch(batch.getId());
                    notifyDataSetChanged();
                }
            });
            holder.batchExpiryDate = (TextView) convertView.findViewById(R.id.batch_exp_date);
            holder.reportedDateView = (TextView) convertView.findViewById(R.id.batch_qty);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);
        holder.batchExpiryDate.setText(sdf.format(listData.get(position).getExpiryDate()));
        holder.reportedDateView.setText(String.valueOf(listData.get(position).getQuantity()));
        return convertView;
    }

    static class ViewHolder {
        ImageView deleteBatchButton;
        TextView batchExpiryDate;
        TextView reportedDateView;
    }
}
