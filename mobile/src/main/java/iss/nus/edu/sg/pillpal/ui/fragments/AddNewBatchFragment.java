package iss.nus.edu.sg.pillpal.ui.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.interfaces.IInventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.ui.activities.InventoryActivity;
import iss.nus.edu.sg.pillpal.utilities.UIUtils;

/**
 * @author Bavithra Thangaraj on 9/1/2016.
 */
public class AddNewBatchFragment extends AddNewMedicineFragment {

    @Bind(R.id.linear)
    LinearLayout linearLayout;
    @Bind(R.id.batch_list)
    RecyclerView batchList;

    @Bind(R.id.linear_list_view)
    LinearLayout mLinearListView;

    private Object name;
    private BatchAdapterSwipe baseAdapter;
    private Pill pill;

    public AddNewBatchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);
        this.mAddBatch.setVisibility(View.VISIBLE);

        return view;
    }

    //getting intent from Inventory Fragment
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getActivity().getIntent().getExtras();
        if (args != null && getActivity().getIntent().getExtras().containsKey("PILL_ID")) {
            name = args.get("PILL_ID");
            InventoryManager iInventoryManager = InventoryManager.get(getActivity());
            pill = iInventoryManager.getPill(Integer.parseInt(name.toString()));
            this.mEditName.setText(pill.getName());
            disableFocus(mEditName);
            this.mEditDetails.setText(pill.getDetails());
            disableFocus(mEditDetails);
            this.mExpDate.setVisibility(View.GONE);
            this.mCalender.setVisibility(View.GONE);
            this.mEditQuantity.setVisibility(View.GONE);
            this.mAddPill.setVisibility(View.GONE);
            this.mReset.setVisibility(View.GONE);
            this.mSpinner.setVisibility(View.GONE);
            this.mQtyThreshold.setText(String.valueOf(pill.getThresholdQuantity()));
            this.mExpThreshold.setText(String.valueOf(pill.getNumberOfDaysThresholdForExpiry()));
            onEditTextChange(mQtyThreshold);
            onEditTextChange(mExpThreshold);

            batchList.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            batchList.setLayoutManager(mLayoutManager);

            //baseAdapter = new BatchAdapter(AddNewBatchFragment.this.getActivity(), getContext(), pill.getBatches());
            baseAdapter = new BatchAdapterSwipe(pill.getBatches(),getContext());
            batchList.setAdapter(baseAdapter);
            if (batchList != null) {
                this.batchList.setVisibility(View.VISIBLE);
                this.mLinearListView.setVisibility(View.VISIBLE);
            }
            baseAdapter.notifyDataSetChanged();
        }
        initSipwDelete();

    }

    private void onEditTextChange(EditText mChangedEditText) {

        mChangedEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0){
                    InventoryManager iInventoryManager = InventoryManager.get(getActivity());
                    iInventoryManager.updateThresholdValuesOfPill(pill.getId(), Integer.parseInt(mQtyThreshold.getText().toString()),Integer.parseInt(mExpThreshold.getText().toString()));
                }
            }
        });
    }


    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.add_batch_Button:
                addBatch();
                break;
        }

    }

    private void addBatch() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.add_row, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        final EditText batchExp = (EditText) promptsView.findViewById(R.id.edit_expiry);
        final EditText batchQty = (EditText) promptsView.findViewById(R.id.edit_quantity);
        final ImageView batchCalender = (ImageView) promptsView.findViewById(R.id.btn_calender);
        final Batch batch = new Batch();

        batchCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                batchExp.setText(UIUtils.selectCalender(AddNewBatchFragment.this,batchExp));
            }
        });

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                mEditQuantity.setText(batchQty.getText());
                                try {
                                    batch.setExpiryDate(UIUtils.simpleDateFormat.parse(batchExp.getText().toString()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(batchQty.getText().toString().trim().length() > 0 &&
                                        batchExp.getText().toString().trim().length() > 0) {
                                    batch.setQuantity(Integer.parseInt(batchQty.getText().toString()));
                                    InventoryManager iInventoryManager = InventoryManager.get(getActivity());
                                    pill = iInventoryManager.addBatch(Integer.parseInt(name.toString()), batch);
                                    if(pill != null) {
                                        baseAdapter.swapItems(pill.getBatches());
                                        // baseAdapter.notifyDataSetChanged();
                                        batchList.setAdapter(baseAdapter);
                                    } else {
                                        Toast.makeText(getContext(), "Please fill in with valid values", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Please fill in necessary fields", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    //disable focus while intent from Inventory
    private void disableFocus(EditText editText) {
        editText.setFocusable(false);
        editText.setClickable(false);
        editText.setFocusableInTouchMode(false);
        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.add_pill_disable));
//        mImage.setColorFilter(Color.argb(220, 220, 220, 240));
        mVoice.setColorFilter(Color.argb(220, 220, 220, 255));
        this.mVoice.setEnabled(false);
//        this.mImage.setEnabled(false);

    }




    public class BatchAdapterSwipe extends RecyclerView
            .Adapter<BatchAdapterSwipe
            .DataObjectHolder> {

        private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

        List<Batch>  itemsPendingRemoval = new ArrayList<Batch>();
        int lastInsertedIndex; // so we can add some more items for testing purposes
        boolean undoOn; // is undo on, you can turn it on from the toolbar menu

        private Handler handler = new Handler(); // hanlder for running delayed runnables
        HashMap<Batch, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be


        private  String LOG_TAG = "MyRecyclerViewAdapter";
        private ArrayList<Batch> mDataset;


        public  class DataObjectHolder extends RecyclerView.ViewHolder
                implements View
                .OnClickListener {
            TextView batchExpiryDate;
            TextView reportedDateView;
            Button UndoBtn;
            LinearLayout undoLayout;


            public DataObjectHolder(View itemView)
            {
                super(itemView);
                batchExpiryDate = (TextView) itemView.findViewById(R.id.batch_exp_date);
                reportedDateView = (TextView) itemView.findViewById(R.id.batch_qty);
                UndoBtn = (Button) itemView.findViewById(R.id.undo_btn);
                undoLayout = (LinearLayout) itemView.findViewById(R.id.undo_layout);

            }

            @Override
            public void onClick(View v) {

            }
        }



        public BatchAdapterSwipe(ArrayList<Batch> myDataset,Context context) {
            mDataset = myDataset;

        }
        View view;
        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_batch, parent, false);
            this.view = view;

            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(DataObjectHolder holder, final int position)
        {
            final Batch batch  = mDataset.get(position);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

            if (itemsPendingRemoval.contains(batch)) {
                // we need to show the "undo" state of the row
                holder.itemView.setBackgroundColor(Color.parseColor("#ff3232"));
                holder.batchExpiryDate.setVisibility(View.GONE);
                holder.reportedDateView.setVisibility(View.GONE);
                holder.undoLayout.setVisibility(View.VISIBLE);
                holder.UndoBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(batch);
                        pendingRunnables.remove(batch);
                        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(batch);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(mDataset.indexOf(batch));
                    }
                });
            } else {
                // we need to show the "normal" state
                holder.itemView.setBackgroundColor(Color.WHITE);
                holder.batchExpiryDate.setVisibility(View.VISIBLE);
                holder.reportedDateView.setVisibility(View.VISIBLE);
                holder.undoLayout.setVisibility(View.GONE);

                holder.batchExpiryDate.setText(sdf.format(mDataset.get(position).getExpiryDate()));
                holder.reportedDateView.setText(String.valueOf(mDataset.get(position).getQuantity()));


            }



        }

        public void swapItems(ArrayList<Batch> items) {
            this.mDataset.clear();
            this.mDataset = items;
//        this.listData.addAll(items);
            notifyDataSetChanged();
        }

        public void addItem( Batch dataObj, int index) {
            mDataset.add(index, dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

        public void setUndoOn(boolean undoOn) {
            this.undoOn = undoOn;
        }

        public boolean isUndoOn() {
            return undoOn;
        }

        public void pendingRemoval(int position) {
            final Batch item = mDataset.get(position);
            if (!itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.add(item);
                // this will redraw row in "undo" state
                notifyItemChanged(position);
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        IInventoryManager inventoryManager = InventoryManager.get(getActivity());
                        inventoryManager.deleteBatch(item.getId());
                        // Update the UI now.
                        remove(mDataset.indexOf(item));
                        if(mDataset.isEmpty()){
                            Intent intent = new Intent(getActivity(), InventoryActivity.class);
                            startActivity(intent);
                        }
                    }
                };
                pendingRunnables.put(item, pendingRemovalRunnable);
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            }
        }

        public void remove(int position) {
            Batch item = mDataset.get(position);
            if (itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.remove(item);
            }
            if (mDataset.contains(item)) {
                mDataset.remove(position);
                notifyItemRemoved(position);
            }
        }

        public boolean isPendingRemoval(int position)
        {
            Batch item = mDataset.get(position);
            return itemsPendingRemoval.contains(item);
        }


    }


    void initSipwDelete()
    {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {


            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.parseColor("#ff3232"));
                xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_clear_24dp);
                xMark.setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                BatchAdapterSwipe testAdapter = (BatchAdapterSwipe)recyclerView.getAdapter();
                testAdapter.setUndoOn(true);
                if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                BatchAdapterSwipe adapter = (BatchAdapterSwipe)batchList.getAdapter();
                boolean undoOn = adapter.isUndoOn();
                if (undoOn) {
                    adapter.pendingRemoval(swipedPosition);
                } else {
                    adapter.remove(swipedPosition);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                /*if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }*/

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(batchList);
    }

}


