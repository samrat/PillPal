package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.AddProfileFragment;


public class AddProfileActivity extends BaseSingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new AddProfileFragment();
    }
}
