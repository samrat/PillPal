package iss.nus.edu.sg.pillpal.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Samrat on 13/5/16.
 */
public class PillPalBaseHelper extends SQLiteOpenHelper {

    /**************************************************/
    // Constants
    /**************************************************/
    // The initial version of the application
    private static final int VERSION = 1;
    // Name of database
    private static final String DATABASE_NAME = "pillPal.db";
    // Tag for logging
    private static final String TAG = "PillPalBaseHelper";

    /**************************************************/
    // Instance Variables
    /**************************************************/

    private static String DB_PATH = "";
    private SQLiteDatabase mDataBase;
    private final Context mContext;

    /**************************************************/
    // Constructor
    /**************************************************/
    public PillPalBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }

    /**************************************************/
    // Abstract Method Implementation
    /**************************************************/
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // The dB got upgraded
    }

    /**************************************************/
    // Public Methods
    /**************************************************/
    public void createDB() throws IOException
    {
        //If the database does not exist, copy it from the assets.

        boolean mDataBaseExist = checkDB();
        if(!mDataBaseExist)
        {
            this.getReadableDatabase();
            this.close();
            try
            {
                //Copy the database from assets
                copyDB();
                Log.d(TAG, "Database successfully created.");
            }
            catch (IOException mIOException)
            {
                throw new Error("Error Copying DataBase");
            }
        }
    }

    public boolean openDB() throws SQLException
    {
        String mPath = DB_PATH + DATABASE_NAME;
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    @Override
    public synchronized void close()
    {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

    /**************************************************/
    // Private Methods
    /**************************************************/

    private boolean checkDB()
    {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDB() throws IOException
    {
        InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
        String outFileName = DB_PATH + DATABASE_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }
}
