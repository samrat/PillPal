package iss.nus.edu.sg.pillpal.services;

import iss.nus.edu.sg.pillpal.models.ScheduledTask;
import iss.nus.edu.sg.pillpal.scheduler.Scheduler;
import iss.nus.edu.sg.pillpal.scheduler.tasks.PillExpireTask;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class PillExpireService extends Service {

	public static final long serialVersionUID = 6947813202347920134L;
	public static PillExpireService context = null;
	
    @Override
    public IBinder onBind(Intent intent) {
    	return null;
    }

    @Override
    public void onCreate() {
    	context = PillExpireService.this;
    	super.onCreate();
    }
    
    @Override
    public void onDestroy() {
    	Scheduler.stop(serialVersionUID);
    	super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	ScheduledTask scheduledTask = new ScheduledTask();
    	scheduledTask.setClassName(PillExpireTask.class.getName());
    	scheduledTask.setId(serialVersionUID);
    	SharedPreferences settings = getSharedPreferences("TIME", 0);
    	scheduledTask.setDailyAt(settings.getString("EH", "9")+":"+settings.getString("EM", "00"));
    	scheduledTask.setTaskName(PillExpireTask.class.getName());
    	// add task to scheduler and start it based on the interval value or daily at
    	Scheduler.addTask(scheduledTask);
        return super.onStartCommand(intent, flags, startId);
    }
}