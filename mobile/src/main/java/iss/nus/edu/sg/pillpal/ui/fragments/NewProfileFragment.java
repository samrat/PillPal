package iss.nus.edu.sg.pillpal.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.ui.activities.AddProfileActivity;


public class NewProfileFragment extends Fragment {


    /*
        Public constructors
     */
    public NewProfileFragment() {
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_profile, container, false);

        FloatingActionButton myFab = (FloatingActionButton) view.findViewById(R.id.imageButton);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddProfileActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });
        return view;
    }







}
