package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.interfaces.IProfileManager;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.ui.fragments.NewProfileFragment;
import iss.nus.edu.sg.pillpal.ui.fragments.ProfileFragment;


public class ProfileActivity extends BaseSingleFragmentActivity {

    protected Fragment createFragment() {
        IProfileManager profileManager = ProfileManager.get(this);
        List<Profile> profileList = profileManager.getAllProfiles();

        if(profileList.size() >= 1) {
            return new ProfileFragment();

        }

        return new NewProfileFragment();
    }
}
