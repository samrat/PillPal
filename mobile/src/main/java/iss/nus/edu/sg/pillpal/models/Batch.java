package iss.nus.edu.sg.pillpal.models;

import java.util.Date;

/**
 * Created by Samrat on 26/5/16.
 */
public class Batch {

    /**************************************************/
    // Instance Variables
    /**************************************************/
    /**
     * The id of the batch
     */
    private int mId;

    /**
     * The id of the pill that the batch is associated with.
     */
    private int pillId;

    /**
     * The expiry date of the batch
     */
    private Date mExpiryDate;

    /**
     * The quantity associated with this batch.
     */
    private int mQuantity;

    /**
     * Whether the current batch is expired or not.
     */
    private boolean mIsExpired;

    /**************************************************/
    // Constructors
    /**************************************************/

    public Batch() {

    }

    public Batch(Date expiryDate, int quantity) {
        this(0, expiryDate,quantity);
    }


    public Batch(int id, Date expiryDate, int quantity) {
        this.mId = id;
        this.mExpiryDate = expiryDate;
        this.mQuantity = quantity;
    }

    /**************************************************/
    // Getters & Setters
    /**************************************************/

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Date getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        mExpiryDate = expiryDate;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public boolean isExpired() {
        return mIsExpired;
    }

    public void setExpired(boolean expired) {
        mIsExpired = expired;
    }

    public int getPillId() {
        return pillId;
    }

    public void setPillId(int pillId) {
        this.pillId = pillId;
    }
}
