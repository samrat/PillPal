package iss.nus.edu.sg.pillpal;

import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.models.Profile;

import java.util.List;
import java.util.UUID;

import android.test.AndroidTestCase;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class ProfileManagerTesting extends AndroidTestCase {

	private static ProfileManager manager;

    @Override
    protected void setUp() throws Exception {
    	super.setUp();
    	manager = ProfileManager.get(mContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        manager = null;
    }
    
    public void testAddProfile(){
    	Profile profile = new Profile();
    	profile.setAge(30);
    	profile.setAllergy("None");
    	profile.setName(UUID.randomUUID().toString());
    	profile = manager.addProfile(profile);
        assertTrue(profile.getId() > 0);
    }
    
    public void testGetAllProfiles(){
    	Profile profile = new Profile();
    	profile.setAge(30);
    	profile.setAllergy("None");
    	profile.setName(UUID.randomUUID().toString());
    	manager.addProfile(profile);
    	profile = new Profile();
    	profile.setAge(30);
    	profile.setAllergy("None");
    	profile.setName(UUID.randomUUID().toString());
    	manager.addProfile(profile);
    	List<Profile> lstAll = manager.getAllProfiles();
        assertTrue(lstAll.size() == 2);
    }
    
    public void testDeleteProfile(){
    	List<Profile> lstAll = manager.getAllProfiles();
    	for (Profile profile: lstAll) {
    		manager.deleteProfile(profile.getId());
    	}
    	assertTrue(manager.getAllProfiles().size() == 0);
    }
    
    public void testGetProfile(){
    	Profile profile = new Profile();
    	profile.setAge(30);
    	profile.setAllergy("None");
    	profile.setName(UUID.randomUUID().toString());
    	profile = manager.addProfile(profile);
    	
    	Profile profile2 = manager.getProfile(profile.getId());
    	
        assertTrue(profile2 != null);
        assertTrue(profile2.getName().equalsIgnoreCase(profile.getName()));
    }
}
