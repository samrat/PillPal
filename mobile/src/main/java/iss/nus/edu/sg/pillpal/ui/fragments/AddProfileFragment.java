package iss.nus.edu.sg.pillpal.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;

/**
 * Created by Samrat on 9/9/16.
 */
public class AddProfileFragment extends Fragment {

    /****************************************************/
    // Instance variables
    /****************************************************/
    @Bind(R.id.input_name)
    EditText mName;
    @Bind(R.id.input_age)
    EditText age;
    @Bind(R.id.input_allergy)
    EditText allergy;
    @Bind(R.id.btn_add)
    Button add;

    /****************************************************/
    // Activity Lifecycle

    /****************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_profile, container, false);
        //Binding Butter Knife
        ButterKnife.bind(this, rootView);

        /* Onclick listener for each fields */
        setOnFocusChangeListenerForEditText(mName, "Please enter name");
        setOnFocusChangeListenerForEditText(age, "Please enter age");
        setOnFocusChangeListenerForEditText(allergy, "Please enter allergy details");

        // Add the actions
        addActions();

        return rootView;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }
    /****************************************************/
    // Helper Methods
    /****************************************************/

    /**
     * Method to add the onFocusChangeListener to the EditText of the class.
     *
     * @param text         The EditText item for which the listener needs to be added.
     * @param errorMessage The error message in case the validation fails.
     */
    private void setOnFocusChangeListenerForEditText(final EditText text, final String errorMessage) {
        text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (text.getText().length() <= 0) {
                    text.setError(errorMessage);
                }
            }
        });
    }

    /**
     * Method to add actions to the various components.
     */
    private void addActions() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile profile = null;
                if (mName.getText().length() <= 0 || age.getText().length() <= 0 || allergy.getText().length() <= 0) {
                    Toast.makeText(getActivity(), "Please enter all the details", Toast.LENGTH_SHORT).show();
                    return;
                }

                profile = new Profile();
                profile.setName(mName.getText().toString());
                profile.setAge(Integer.parseInt(age.getText().toString()));
                profile.setAllergy(allergy.getText().toString());

                addProfileToDatabase(profile);
            }
        });
    }

    /**
     * Method to add profile to the database.
     * @param profile The profile that needs to be added to the dB.
     */
    private void addProfileToDatabase(Profile profile) {

        if(ProfileManager.get(getActivity()).addProfile(profile) != null) {
            Toast.makeText(getActivity(), "Profile Added successfully", Toast.LENGTH_SHORT).show();
            mName.setText("");
            age.setText("");
            allergy.setText("");
            mName.requestFocus();
        } else {
            Toast.makeText(getActivity(), "Profile is not added. Please check the entered details", Toast.LENGTH_SHORT).show();
        }
    }
}
