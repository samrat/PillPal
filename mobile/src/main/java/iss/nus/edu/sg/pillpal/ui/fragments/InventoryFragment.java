package iss.nus.edu.sg.pillpal.ui.fragments;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.ui.activities.AddMedicine;
import iss.nus.edu.sg.pillpal.ui.activities.BaseSingleFragmentActivity;
import iss.nus.edu.sg.pillpal.ui.activities.HomeActivity;
import iss.nus.edu.sg.pillpal.utilities.CustomChooser;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;
import iss.nus.edu.sg.pillpal.utilities.PictureUtility;

/**
 * Created by Samrat on 17/5/16.
 */
public class InventoryFragment extends Fragment implements CustomChooser.ChooserCallback{
    
    /****************************************************/
    // Instance variables
    /****************************************************/
    private ExpandableListAdapter mListAdapter;
    @Bind(R.id.inventory_expandable_ListView)
    ExpandableListView mExpandableListView;
    @Bind(R.id.add_medicine)
    FloatingActionButton myFab;

    ImageView mHomeButton;
    Spinner mSpinner;

    //@Bind(R.id.enter_pill)
    FloatingActionButton myFab2;

    Animation FabRcloclwise,FabAclockwise;
    private List<Pill> mAllPills;
    public Dialog dialog;

    /****************************************************/
    // Constructor
    /****************************************************/
    public InventoryFragment() {
        // Required empty public constructor
    }

    /****************************************************/
    // View Lifecycle

    /****************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_inventory, container, false);

        // Butterknife bind
        ButterKnife.bind(this, view);

        FabRcloclwise = AnimationUtils.loadAnimation(getContext(),R.anim.fab_clockwise);
        FabAclockwise = AnimationUtils.loadAnimation(getContext(),R.anim.fab_aclockwise);
        final CustomChooser customChooser =  new CustomChooser(this);

        myFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // creating custom Floating Action button
                customChooser.CustomDialog(InventoryFragment.this);
            }
        });

        // Call the dB to get all the pills.
        mAllPills = InventoryManager.get(getActivity()).getAllPills();
        // Create the adapter
        mListAdapter = new ExpandableListAdapter(getActivity(), mAllPills);
        // Set the adapter to the list view.
        mExpandableListView.setAdapter(mListAdapter);
        // Customize action bar
        customizeActionBar();
        // Add actions
        addButtonAndSpinnerAction();
        // Set the listener for the taps
        addListenerToListView();
        // Notification arrival check
        checkIfNavigatingFromNotification();

        return view;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }

    @Override
    public void onResume() {
        // The below has been added since we need to refresh every time the view is displayed.
        super.onResume();
        refreshExpandableView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    /****************************************************/
    // Private Methods
    /****************************************************/
    /**
     * This will disable the automatic click action of the complete group.
     */
    private void addListenerToListView() {
        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                //Do some other stuff, but you shall not expand or collapse
                Intent intent = new Intent(getActivity(), AddMedicine.class);
                intent.putExtra("PILL_ID", mListAdapter.getPills().get(groupPosition).getId());
                startActivity(intent);
             /* Animation*/
                NavigationUtility.animateLeft(getActivity());
                return true;
            }
        });
    }


    /**
     * Wire up the actions pertaining to the button & spinner.
     */
    private void addButtonAndSpinnerAction() {
        // Spinner action
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                       int position, long id) {
                // your code here
                switch (position) {
                    case 0:
                        mListAdapter.setPills(InventoryManager.get(getActivity()).getAllPills());
                        break;
                    case 1:
                        mListAdapter.setPills(InventoryManager.get(getActivity()).getExpiredMedicines());
                        break;
                    case 2:
                        mListAdapter.setPills(InventoryManager.get(getActivity()).getOutOfStock());
                        break;
                    default:
                        mListAdapter.setPills(InventoryManager.get(getActivity()).getAllPills());
                        break;
                }
                mListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        // Now add the home action
        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(InventoryFragment.this.checkAndroidStack() <= 1) {
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    startActivity(intent);
                }
                getActivity().finish();
                InventoryFragment.this.getActivity().overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
            }
        });
    }


    /**
     * Method to check if the fragment has been traversed from one of the notifications.
     * If it is navigating from one of the notifications, the appropriate screen is launched.
     */
    private void checkIfNavigatingFromNotification() {
        // Check if we are coming from a notification or not.
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("NOTIFICATION",0);
        String string = sharedPreferences.getString("NOTIFICATION", "DEFAULT");
        if (!string.equals("DEFAULT")) {
            if (string.equals("EXPIRY")) {
                mSpinner.setSelection(1);
            } else if (string.equals("OUT_OF_STOCK")) {
                mSpinner.setSelection(2);
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("NOTIFICATION");
            editor.commit();
        }
    }

    /**
     * Check the number of activities that are present in the activity stack.
     * @return The number of activities present in the stack.
     */
    private int checkAndroidStack(){
        int numOfActivities = 0;
        ActivityManager m = (ActivityManager) getActivity()
                .getSystemService(getActivity().ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfoList = m.getRunningTasks(10);
        Iterator<ActivityManager.RunningTaskInfo> itr = runningTaskInfoList.iterator();
        while (itr.hasNext()) {
            ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo) itr.next();
            numOfActivities = runningTaskInfo.numActivities;
            break;
        }
        return numOfActivities;
    }

    /**
     * Method to customize the action bar.
     */
    private void customizeActionBar() {
        BaseSingleFragmentActivity baseSingleFragmentActivity = (BaseSingleFragmentActivity) getActivity();
        ViewGroup viewGroup = (ViewGroup) baseSingleFragmentActivity.getLinearLayout().getParent();
        viewGroup.removeView(baseSingleFragmentActivity.getLinearLayout());
        View customizedActionBarContent = getActivity().getLayoutInflater().inflate(R.layout.content_inventory_action_bar, viewGroup, false);

        mHomeButton = (ImageView) customizedActionBarContent.findViewById(R.id.inventory_btn_home);
        mSpinner = (Spinner) customizedActionBarContent.findViewById(R.id.inventory_spinner);

        viewGroup.addView(customizedActionBarContent);
    }

    /**
     * Method called once the activity returns after taking a photo using camera or choose a image file from gallery.
     * @param requestCode The request code for opening the resource.
     * @param resultCode The result code. -1 means everything succeeded.
     * @param data The attached intent providing data from the opened resource.
     */
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PictureUtility.CAMERA_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), AddMedicine.class);
            intent.putExtra(AddMedicine.PICTURE_PATH, PictureUtility.getCameraFileUri());
            getActivity().startActivity(intent);
        } else if(requestCode == PictureUtility.GALLERY_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), AddMedicine.class);
            intent.putExtra(AddMedicine.PICTURE_PATH, data.getData());
            getActivity().startActivity(intent);
        } else {//TODO - This needs to be checked properly.
            //Toast message for failure
            Toast.makeText(getContext(), "Fail to process the image", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Check for network connectivity
     * @return
     */
    public boolean checkConnectivity(){
        ConnectivityManager cm =
                (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    /**
     * Refresh the content of the expandable view.
     */
    private void refreshExpandableView(){
        int selectedPosition = mSpinner.getSelectedItemPosition();
        switch (selectedPosition) {
            case 0:
                mListAdapter.setPills(InventoryManager.get(getActivity()).getAllPills());
                break;
            case 1:
                mListAdapter.setPills(InventoryManager.get(getActivity()).getExpiredMedicines());
                break;
            case 2:
                mListAdapter.setPills(InventoryManager.get(getActivity()).getOutOfStock());
                break;
            default:
                mListAdapter.setPills(InventoryManager.get(getActivity()).getAllPills());
                break;
        }
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void callback() {
        Intent i = new Intent(getActivity(), AddMedicine.class);
        startActivity(i);
        NavigationUtility.animateLeft(getActivity());
    }
    /****************************************************/
        // Adapter class
        /****************************************************/
        public class ExpandableListAdapter extends BaseExpandableListAdapter {

            /****************************************************/
            // ExpandableListAdapter - Instance Variable
            /****************************************************/
            private Context mContext;
            private List<Pill> mPills;

            /****************************************************/
            // ExpandableListAdapter - Constructor

            /****************************************************/

            public ExpandableListAdapter(Context context, List<Pill> pills) {
                this.mContext = context;
                this.mPills = pills;
            }

            /****************************************************/
            // ExpandableListAdapter - Public Methods

            /****************************************************/
            public void setPills(List<Pill> pills) {
                mPills = pills;
            }

            public List<Pill> getPills() {
                return mPills;
            }
            /****************************************************/
            // ExpandableListAdapter - List Data Source

            /****************************************************/
            @Override
            public Object getChild(int groupPosition, int childPosition) {
                return this.mPills.get(groupPosition).getBatches().get(childPosition);
            }

            @Override
            public long getChildId(int groupPosition, int childPosition) {
                return childPosition;
            }

            @Override
            public View getChildView(int groupPosition, final int childPosition,
                                     boolean isLastChild, View convertView, ViewGroup parent) {

                final Batch batch = (Batch) getChild(groupPosition, childPosition);

                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) this.mContext
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    //convertView = inflater.inflate(R.layout.inventory_list_item_cell, null);
                }


                return updateChildView(groupPosition, childPosition, convertView);
            }

            @Override
            public int getChildrenCount(int groupPosition) {
                return mPills.get(groupPosition).getBatches().size();
            }

            @Override
            public Object getGroup(int groupPosition) {
                return mPills.get(groupPosition);
            }

            @Override
            public int getGroupCount() {
                return mPills.size();
            }

            @Override
            public long getGroupId(int groupPosition) {
                return groupPosition;
            }

            @Override
            public View getGroupView(int groupPosition, boolean isExpanded,
                                     View convertView, ViewGroup parent) {

                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) this.mContext
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.inventory_list_item_section, null);
                }
                return updateGroupView(groupPosition, isExpanded, convertView, parent);
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public boolean isChildSelectable(int groupPosition, int childPosition) {
                return true;
            }

            /****************************************************/
            // ExpandableListAdapter - Private Methods

            /****************************************************/

            private View updateGroupView(final int groupPosition, final boolean isExpanded,
                                         View groupView, final ViewGroup parent) {

                groupView = resetGroupView(groupView);

                Pill pill = (Pill) getGroup(groupPosition);

                TextView txtPillName = (TextView) groupView.findViewById(R.id.list_section_inventory_name_text_view);
                TextView txtPillDetails = (TextView) groupView.findViewById(R.id.list_section_inventory_details_text_view);
                TextView txtPillExpiry = (TextView) groupView.findViewById(R.id.list_section_inventory_expiry_text_view);
                TextView txtPillQuantity = (TextView) groupView.findViewById(R.id.list_section_inventory_quantity_text_view);
                TextView txtPillUnit = (TextView) groupView.findViewById(R.id.list_section_inventory_unit_text_view);
//                final ImageView listHeaderArrow = (ImageView) groupView.findViewById(R.id.list_section_inventory_image_view);
                final ImageView listHeaderArrow = (ImageView) groupView.findViewById(R.id.first_letter_view);


                /********************
                 *
                 * UI MODIFICATION LIST BY DEEPAK
                 *
                 *
                 *
                 **********************/

                Typeface font = Typeface.createFromAsset(
                        groupView.getContext().getAssets(),
                        "Fonts/SegoeSemibold.ttf");
                txtPillName.setTypeface(font);
                txtPillDetails.setTypeface(font);
                txtPillExpiry.setTypeface(font);



                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
                // generate random color
                int color1 = generator.getColor("614dtf");
                // generate color based on a key (same key returns the same color), useful for list/grid views

                // declare the builder object once.
                TextDrawable.IBuilder builder = TextDrawable.builder()
                        .beginConfig()
                        .endConfig()
                        .roundRect(2);
                String firstLetter = pill.getName().substring(0, 1);
                // reuse <></>he builder specs to create multiple drawables
                TextDrawable ic1 = builder.build(firstLetter, color1);
                listHeaderArrow.setImageDrawable(ic1);









                /***********END OF NEW LIST**********************/

                /*****
                 *
                 * uncomment
                 *
                 */

               /* listHeaderArrow.setVisibility(View.GONE);*/



                txtPillName.setText(pill.getName());
                txtPillDetails.setText(pill.getDetails());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                txtPillExpiry.setText("Expiry:  " +dateFormat.format(pill.getExpiryDate()));
                txtPillQuantity.setText(String.valueOf(pill.getQuantity()));
                txtPillUnit.setText(pill.getUnit());

                //final int imageResourceId = isExpanded ? R.drawable.list_up : R.drawable.list_down;
//                listHeaderArrow.setImageResource(imageResourceId);
//                listHeaderArrow.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (isExpanded)
//                            ((ExpandableListView) parent).collapseGroup(groupPosition);
//                        else((ExpandableListView) parent).expandGroup(groupPosition, true);
//
//                    }
//                });

                if (pill.isExpired()) {
                    txtPillExpiry.setTextColor(Color.parseColor("#ff0000"));
                }

                if (pill.isOutOfStock()) {
                    txtPillQuantity.setTextColor(Color.parseColor("#ee7600"));
                }
                return groupView;
            }

            /**
             * Update the child view with respect to the batch.
             * @param groupPosition The section for which the batch needs to be displayed.
             * @param childPosition The position of the child view.
             * @param childView The child view that needs to be updated.
             * @return The updated child view.
             */
            private View updateChildView(int groupPosition, final int childPosition, View childView) {

                childView = resetChildView(childView);

                final Batch batch = (Batch) getChild(groupPosition, childPosition);

                TextView txtBatchExpiry = (TextView) childView
                        .findViewById(R.id.inventory_cell_batch_expiry);
                TextView txtBatchQuantity = (TextView) childView
                        .findViewById(R.id.inventory_cell_batch_quantity);
                /**
                 * Trash icon is removed as per Sprint3 plan
                 */
//                ImageView imgDeleteBatch = (ImageView) childView.findViewById(R.id.delete_batch);
//
//                imgDeleteBatch.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        InventoryManager.get(InventoryFragment.this.getActivity()).deleteBatch(batch.getId());
//                        InventoryFragment.this.refreshExpandableView();
//                    }
//                });

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                txtBatchExpiry.setText(dateFormat.format(batch.getExpiryDate()));

                txtBatchQuantity.setText(String.valueOf(batch.getQuantity()));

                if (batch.isExpired()) {
                    txtBatchExpiry.setTextColor(Color.RED);
                    txtBatchQuantity.setTextColor(Color.RED);
                }

                return childView;
            }

            /**
             * Reset the child View to its initial state.
             * @param childView The child view that needs to be updated.
             * @return The child view that needs to be reset to its initial state.
             */
            private View resetChildView(View childView) {
                TextView txtBatchExpiry = (TextView) childView
                        .findViewById(R.id.inventory_cell_batch_expiry);
                TextView txtBatchQuantity = (TextView) childView
                        .findViewById(R.id.inventory_cell_batch_quantity);
                txtBatchExpiry.setText("");
                txtBatchQuantity.setText("");

                txtBatchExpiry.setTextColor(Color.BLACK);
                txtBatchQuantity.setTextColor(Color.BLACK);

                return childView;
            }

            /**
             * Reset the group View to its initial state.
             * @param groupView The group view that needs to be updated.
             * @return The group view that needs to be reset to its initial state.
             */
            private View resetGroupView(View groupView) {
                TextView txtPillName = (TextView) groupView.findViewById(R.id.list_section_inventory_name_text_view);
                TextView txtPillDetails = (TextView) groupView.findViewById(R.id.list_section_inventory_details_text_view);
                TextView txtPillExpiry = (TextView) groupView.findViewById(R.id.list_section_inventory_expiry_text_view);
                TextView txtPillQuantity = (TextView) groupView.findViewById(R.id.list_section_inventory_quantity_text_view);

                txtPillName.setText("");
                txtPillDetails.setText("");
                txtPillExpiry.setText("");
                txtPillQuantity.setText("");

                txtPillName.setTextColor(Color.BLACK);
                txtPillDetails.setTextColor(Color.BLACK);
                txtPillExpiry.setTextColor(Color.BLACK);
                txtPillQuantity.setTextColor(Color.BLACK);
                return groupView;
            }
        }
    }
