package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import iss.nus.edu.sg.pillpal.R;

public class RoutineActivity extends BaseAbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine);
    }
}
