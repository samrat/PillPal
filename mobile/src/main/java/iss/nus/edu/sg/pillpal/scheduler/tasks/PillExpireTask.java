package iss.nus.edu.sg.pillpal.scheduler.tasks;

import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.services.PillExpireService;
import iss.nus.edu.sg.pillpal.utilities.PillNotificationUtility;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class PillExpireTask extends CommonTask {

	@Override
	public void runTask() throws Exception {
		InventoryManager manager = InventoryManager.get(PillExpireService.context);
		boolean showNotification = false;
		String message = "";
		for (Pill pill: manager.getExpiredMedicines()) {
			message = "Some pills will be expiring soon. Please check!";
			showNotification = true;
		}
		if (showNotification) {
			PillNotificationUtility.notificationExpire("Pills expiring", message, "Click here to see more.");
		}
	}
}
