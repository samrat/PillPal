package iss.nus.edu.sg.pillpal.interfaces;

import java.util.List;

import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;

/**
 * Created by Samrat on 13/5/16.
 */
public interface IInventoryManager {

    /**
     * Method to get all pills that are present in the database.
     * This takes into account all the batches of the medicine.
     * It sets the various flags(out of stock, expired, etc) also before returning.
     * @return The list of all pills. If the dB is empty, then an empty list is returned.
     */
    public List<Pill> getAllPills();

    /**
     * Method to add a new pill to the database.
     * This add the pill details & creates the first batch.
     * All the required parameters of the pill need to be passed before successful addition takes place.
     * @param pill The pill that needs to be added to the dB.
     * @return If successful, the pill object that was added to the database, else null.
     */
    public Pill addPill(Pill pill);

    /**
     * Method to update the threshold values of the pill.
     * @param pillId The pill id for which the attributes need to be upgraded.
     * @param thresholdQuantity The threshold quantity that needs to be set for the pill.
     * @param thresholdExpiry The threshold expiry that needs to be set for the pill.
     * @return The updated pill if the update was successful, else null.
     */
    public Pill updateThresholdValuesOfPill(int pillId, int thresholdQuantity, int thresholdExpiry);

    /**
     * Method to update the pill when the user consumes the pill.
     * @param pillId The pill that needs to be updated.
     * @param quantity The quantity that the user has consumed.
     * @return The updated pill if successfully added, else null.
     */
    public Pill updatePillForConsumption(int pillId, int quantity);

    /**
     * Method to add a batch to an existing pill.
     * @param pillId The pill Id that the batch needs to be added to.
     * @param batch The batch object that needs to be added to the pill.
     * @return The updated pill if successfully added, else null.
     */
    public Pill addBatch(int pillId, Batch batch);

    /**
     * Method to delete a batch associated with a pill.
     * If it is the last batch associated with the pill, then the pill is also deleted.
     * @param batchId The batch id that needs to be deleted.
     */
    public boolean deleteBatch(int batchId);

    /**
     * Method to get a particular based on its Id.
     * @param pillId The pill id for which the pill needs to be queried.
     * @return If the id is present in dB the pill, else null.
     */
    public Pill getPill(int pillId);

    /**
     * Method to get only the expired medicines.
     * @return The list of expired medicines.
     */
    public List<Pill> getExpiredMedicines();

    /**
     * Method to get out of stock medicines.
     * @return The list of medicines that are out of stock.
     */
    public List<Pill> getOutOfStock();
}
