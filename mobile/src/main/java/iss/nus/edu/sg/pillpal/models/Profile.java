package iss.nus.edu.sg.pillpal.models;

/**
 * Created by koushik on 5/27/2016.
 */
public class Profile {
    /*
    Instance variables of Profile
     */
    /*
            The name of profile
     */
    private String name;
    /*
        Id of profile
     */
    private Integer id;
    /*
        The age of user profile
     */
    private int age;
    /*
        Allergy of user profile
     */
    private String allergy;

    /**
     * Default Constructor
     */
    public Profile() {

    }

/*
        Public constructor
 */

    public Profile(String name, int id, int age, String allergy) {
        this.name = name;
        this.id = id;
        this.age = age;
        this.allergy = allergy;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }
}
