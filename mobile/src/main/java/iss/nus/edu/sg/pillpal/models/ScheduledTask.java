package iss.nus.edu.sg.pillpal.models;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class ScheduledTask {

	private Long id;
	
	private String taskName;
	
	private String className;
	
	private Long interval = 0l;
	
	private String dailyAt;
		
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTaskName() {
		return taskName;
	}
	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public Long getInterval() {
		return interval;
	}
	
	public void setInterval(Long interval) {
		this.interval = interval;
	}
	
	public String getDailyAt() {
		return dailyAt;
	}
	
	public void setDailyAt(String dailyAt) {
		this.dailyAt = dailyAt;
	}
}