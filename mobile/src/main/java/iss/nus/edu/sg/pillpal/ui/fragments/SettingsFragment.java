package iss.nus.edu.sg.pillpal.ui.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import java.util.List;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.services.PillConsumeService;
import iss.nus.edu.sg.pillpal.services.PillExpireService;
import iss.nus.edu.sg.pillpal.services.PillOutOfStockService;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;

/**
 * Created by Samrat on 9/9/16.
 */
public class SettingsFragment extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the base view
         rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        SharedPreferences settings = getActivity().getSharedPreferences("TIME", 0);
        final SharedPreferences.Editor editor = settings.edit();

        Switch swtExpire = (Switch) rootView.findViewById(R.id.swtExpire);
        if (isServiceRunning(PillExpireService.class.getName())) {
            swtExpire.setChecked(true);
        }

        EditText txtEH = (EditText)rootView.findViewById(R.id.txtEH);
        txtEH.setText(settings.getString("EH", "9"));
        EditText txtEM = (EditText)rootView.findViewById(R.id.txtEM);
        txtEM.setText(settings.getString("EM", "00"));

        // attach a listener to check for changes in state
        swtExpire.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putString("EH", ""+((EditText)rootView.findViewById(R.id.txtEH)).getText());
                    editor.putString("EM", ""+((EditText)rootView.findViewById(R.id.txtEM)).getText());
                    // Commit the edits!
                    editor.commit();
                    getActivity().startService(new Intent(getActivity().getBaseContext(), PillExpireService.class));
                } else {
                    getActivity().stopService(new Intent(getActivity().getBaseContext(), PillExpireService.class));
                }
            }
        });

        Switch swtOOT = (Switch) rootView.findViewById(R.id.swtOOS);
        if (isServiceRunning(PillOutOfStockService.class.getName())) {
            swtOOT.setChecked(true);
        }

        EditText txtOH = (EditText)rootView.findViewById(R.id.txtOH);
        txtOH.setText(settings.getString("OH", "10"));
        EditText txtOM = (EditText)rootView.findViewById(R.id.txtOM);
        txtOM.setText(settings.getString("OM", "00"));

        // attach a listener to check for changes in state
        swtOOT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putString("OH", ""+((EditText)rootView.findViewById(R.id.txtOH)).getText());
                    editor.putString("OM", ""+((EditText)rootView.findViewById(R.id.txtOM)).getText());
                    // Commit the edits!
                    editor.commit();
                    getActivity().startService(new Intent(getActivity().getBaseContext(), PillOutOfStockService.class));
                } else {
                    getActivity().stopService(new Intent(getActivity().getBaseContext(), PillOutOfStockService.class));
                }
            }
        });

        Switch swtPillConsume = (Switch) rootView.findViewById(R.id.swtRoutMed);
        if (isServiceRunning(PillConsumeService.class.getName())) {
            swtPillConsume.setChecked(true);
        }

        EditText txtPR = (EditText)rootView.findViewById(R.id.txtPR);
        txtPR.setText(settings.getString("PRH", "10"));
        EditText txtPRR = (EditText)rootView.findViewById(R.id.txtPRR);
        txtPRR.setText(settings.getString("PRRM", "00"));

        // attach a listener to check for changes in state
        swtPillConsume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putString("PRH", ""+((EditText)rootView.findViewById(R.id.txtPR)).getText());
                    editor.putString("PRRM", ""+((EditText)rootView.findViewById(R.id.txtPRR)).getText());
                    // Commit the edits!
                    editor.commit();
                    getActivity().startService(new Intent(getActivity().getBaseContext(), PillConsumeService.class));
                } else {
                    getActivity().stopService(new Intent(getActivity().getBaseContext(), PillConsumeService.class));
                }
            }
        });

        return rootView;
    }


    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }


    public boolean isServiceRunning(String serviceClassName){
        final ActivityManager activityManager = (ActivityManager)getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            if (runningServiceInfo.service.getClassName().equalsIgnoreCase(serviceClassName)){
                return true;
            }
        }
        return false;
    }

}
