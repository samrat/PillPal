package iss.nus.edu.sg.pillpal.exceptions;

import android.util.Log;

import iss.nus.edu.sg.pillpal.utilities.StringUtility;

/**
 * Created by Samrat on 14/9/16.
 */
public class DuplicateException extends Exception {

    public static final String TAG = "DuplicateException";

    public DuplicateException(String message) {
        super(message);
    }
}
