package iss.nus.edu.sg.pillpal;

import iss.nus.edu.sg.pillpal.data.PrescriptionManager;
import iss.nus.edu.sg.pillpal.models.Prescription;
import android.test.AndroidTestCase;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class PrescriptionManagerTesting extends AndroidTestCase {

	private static PrescriptionManager manager;

    @Override
    protected void setUp() throws Exception {
    	super.setUp();
    	manager = PrescriptionManager.get(mContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        manager = null;
    }
    
    public void testAddPrescription(){
    	Prescription prescription = new Prescription();
    	
    	
    	prescription = manager.addPrescription(prescription);
        assertTrue(prescription.getId() > 0);
    }
}
