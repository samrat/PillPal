package iss.nus.edu.sg.pillpal.scheduler;

import iss.nus.edu.sg.pillpal.models.ScheduledTask;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class Scheduler {

	private static Map<Long, TaskManager> runningTasks = new HashMap<Long, TaskManager>();

	/**
	 * Load and initial tasks,
	 */
	
	public static void addTask(ScheduledTask scheduledTask) {
		TaskManager oneTask = new TaskManager(scheduledTask);
		runningTasks.put(oneTask.getTaskId(), oneTask);
	}

	/**
	 * Stop all schedule task
	 */
	public static void stop() {
		if (runningTasks != null) {
			TaskManager taskManager;
			for (Iterator<Entry<Long, TaskManager>> iter = runningTasks.entrySet().iterator(); iter.hasNext();) {
				taskManager = iter.next().getValue();
				taskManager.cancel();
			}
		}
	}

	/**
	 * Stop one task given by id of task
	 * @param taskId
	 */
	public static void stop(Long taskId) {
		if (runningTasks != null) {
			TaskManager taskManager = findRunningTask(taskId);
			if (taskManager != null) {
				taskManager.cancel();
			}
		}
	}

	/**
	 * 
	 * @param taskId
	 * @return
	 */
	private static TaskManager findRunningTask(Long taskId) {
		if (runningTasks == null) {
			return null;
		}
		return runningTasks.get(taskId);
	}
}