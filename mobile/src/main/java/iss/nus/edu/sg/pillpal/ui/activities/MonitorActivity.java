package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.MonitorFragment;


public class MonitorActivity extends BaseSingleFragmentActivity {

    /****************************************************/
    // Abstract Method Implementation
    /****************************************************/

    protected Fragment createFragment() {
        return new MonitorFragment();
    }
}
