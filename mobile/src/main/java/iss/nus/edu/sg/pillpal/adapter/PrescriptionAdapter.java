package iss.nus.edu.sg.pillpal.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;

/**
 * Created by Suba Raj on 8/18/2016.
 */
public class PrescriptionAdapter extends BaseAdapter {
    private ArrayList<PrescriptionItem> listData;
    private LayoutInflater layoutInflater;
    private Activity mActivity;
    private PrescriptionItem prescriptionItem;

    public PrescriptionAdapter(Activity activity,Context context, ArrayList<PrescriptionItem> listData,PrescriptionItem prescriptionItem) {
        this.layoutInflater = LayoutInflater.from(context);
        this.mActivity = activity;
        this.listData = listData;
        this.prescriptionItem = prescriptionItem;
    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_prescription_item, null);
            holder = new ViewHolder();
            holder.pillTxt = (TextView) convertView.findViewById(R.id.pill);
            holder.dosageText = (TextView) convertView.findViewById(R.id.dosage);
            holder.qtyText = (TextView) convertView.findViewById(R.id.qty);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.pillTxt.setText(prescriptionItem.getPill().getName());
        holder.dosageText.setText(prescriptionItem.getDosage());
        holder.qtyText.setText(prescriptionItem.getRoutine());
        return convertView;
    }

    static class ViewHolder {
        TextView pillTxt;
        TextView dosageText;
        TextView qtyText;
    }
}
