package iss.nus.edu.sg.pillpal.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.PrescriptionManager;
import iss.nus.edu.sg.pillpal.interfaces.IPrescriptionManager;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.ui.activities.AddMedicine;
import iss.nus.edu.sg.pillpal.ui.activities.AddPrescription;
import iss.nus.edu.sg.pillpal.ui.activities.PrescriptionActivity;
import iss.nus.edu.sg.pillpal.utilities.CustomChooser;
import iss.nus.edu.sg.pillpal.utilities.DefaultDividerItemDecoration;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;
import iss.nus.edu.sg.pillpal.utilities.PictureUtility;

/**
 * Created by Koushik
 */
public class PrescriptionListFragment extends Fragment implements CustomChooser.ChooserCallback {

    /*
            Instance Variables
     */
    @Bind(R.id.prescriptionlist_recycler_view)
    RecyclerView mPrescriptionRecyclerView;
    @Bind(R.id.imageButton)
    FloatingActionButton myFab;

    private PrescriptionAdapter mPrescriptionAdapter;
    LinearLayout individualPrescriptionLayout;

    /*
        String Constants
     */

public static final String EXPIRY_DATE = "Expiry : ";
public static final String PRESCRIPTION_DATE = "Date : ";
    /*
        Public Constructor
     */
    public PrescriptionListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_prescription, container, false);
        ButterKnife.bind(this, view);
        mPrescriptionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();

        // Now add the home action
        addButtonActions();

        return view;

    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }


    /*
            Private Methods
     */
    private void updateUI() {
        IPrescriptionManager prescriptionManager = PrescriptionManager.get(getActivity());
        List<Prescription> prescriptionList = prescriptionManager.getPrescriptionsForProfile(Integer.parseInt(getActivity().getIntent().getStringExtra("profile_id")));
        mPrescriptionAdapter = new PrescriptionAdapter(prescriptionList);
        mPrescriptionRecyclerView.setAdapter(mPrescriptionAdapter);
        mPrescriptionRecyclerView.addItemDecoration(new DefaultDividerItemDecoration(getActivity()));
    }

    /**
     * Add actions to the various elements of the screens.
     */
    private void addButtonActions() {
        final CustomChooser customChooser = new CustomChooser(this);

        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                customChooser.CustomDialog(PrescriptionListFragment.this);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PictureUtility.CAMERA_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), AddPrescription.class);
            intent.putExtra(AddMedicine.PICTURE_PATH, PictureUtility.getCameraFileUri());
            intent.putExtra("profile_id", getActivity().getIntent().getStringExtra("profile_id"));
            getActivity().startActivity(intent);
        } else if (requestCode == PictureUtility.GALLERY_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), AddPrescription.class);
            intent.putExtra(AddMedicine.PICTURE_PATH, data.getData());
            intent.putExtra("profile_id", getActivity().getIntent().getStringExtra("profile_id"));
            getActivity().startActivity(intent);
        } else {//TODO - This needs to be checked properly.
            //Toast message for failure
            Toast.makeText(getContext(), "Fail to process the image", Toast.LENGTH_LONG).show();
        }
    }
    /*Method for Animation */

    private void finishActivityWithAnimationLeft() {
        getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void callback() {
        Intent prescriptionIntent = new Intent(getActivity(), AddPrescription.class);
        prescriptionIntent.putExtra("profile_id", getActivity().getIntent().getStringExtra("profile_id"));
        startActivity(prescriptionIntent);
        finishActivityWithAnimationLeft();
    }

    /*
            Prescription  Adapter - To hold list of Prescriptions from the database
     */
    private class PrescriptionAdapter extends RecyclerView.Adapter<PrescriptionHolder> {

        /*
                     Instance Variables - Prescription Adapter
                     */

        List<Prescription> mPrescriptionList;


        /*
                Prescription Adapter -   setter - Set the Prescriptionlist retrived from the db
         */

        public void setPrescriptionList(List<Prescription> prescriptionList) {
            mPrescriptionList = prescriptionList;
        }

        /*
                PrescriptionAdapter - Public Constructor
         */

        public PrescriptionAdapter(List<Prescription> prescriptionList) {
            mPrescriptionList = prescriptionList;
        }

        /*
                Prescription Adapter - View Creation - Abstract Method Implementation
         */
        @Override
        public PrescriptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.fragment_prescription_list, parent, false);
            individualPrescriptionLayout = (LinearLayout) view.findViewById(R.id.individual_prescription);
            individualPrescriptionLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent viewPrescriptionIntent = new Intent(getActivity(), PrescriptionActivity.class);
                    viewPrescriptionIntent.putExtra("PrescriptionList", "PrescriptionList");
                    viewPrescriptionIntent.putExtra("PrescriptionId", Integer.toString((int) v.getTag()));
                    Log.v("PrescriptionId", Integer.toString((int) v.getTag()));
                    startActivity(viewPrescriptionIntent);
                    NavigationUtility.animateLeft(getActivity());
                }
            });
            return new PrescriptionHolder(view);

        }

        @Override
        public void onBindViewHolder(PrescriptionHolder holder, int position) {
            Prescription prescription = mPrescriptionList.get(position);
            holder.bindProfile(prescription);
        }

        public int getItemCount() {

            Log.v("PrescriptionListSize", Integer.toString(mPrescriptionList.size()));
            return mPrescriptionList.size();
        }
    }

    private class PrescriptionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /*
               Prescription Holder - Instance Variables
         */

        private TextView mDoctorName;
        private TextView mPrescriptionExpiryDate;
        private TextView mPrescriptionDate;
        private TextView mClinicName;
        private ImageView FirstLetterView;

        public PrescriptionHolder(View itemView) {
            super(itemView);
            mDoctorName = (TextView) itemView.findViewById(R.id.list_item_doctor_name);
            mPrescriptionExpiryDate = (TextView) itemView.findViewById(R.id.list_item_prescription_expiry);
            mPrescriptionDate = (TextView) itemView.findViewById(R.id.list_item_prescription_date);
            mClinicName = (TextView) itemView.findViewById(R.id.list_item_clinic_name);
            individualPrescriptionLayout = (LinearLayout) itemView.findViewById(R.id.individual_prescription);
            FirstLetterView = (ImageView) itemView.findViewById(R.id.first_letter_view);
        }

        /*
            Method to bind Prescription details to the view
         */
        public void bindProfile(Prescription prescription) {
            mDoctorName.setText(prescription.getDoctorName());
            String formattedExpiryDate = formatDate(prescription.getPrescriptionExpiryDate());
            String formattedPrescriptionDate = formatDate(prescription.getPrescriptionDate());
            mPrescriptionExpiryDate.setText(EXPIRY_DATE + formattedExpiryDate);
            mPrescriptionDate.setText(PRESCRIPTION_DATE + formattedPrescriptionDate);
            mClinicName.setText(prescription.getClinicName());
            individualPrescriptionLayout.setTag(prescription.getId());
/********************
 *
 * UI MODIFICATION LIST BY DEEPAK
 *
 *
 *
 **********************/

            Typeface font = Typeface.createFromAsset(
                    itemView.getContext().getAssets(),
                    "Fonts/SegoeSemibold.ttf");
            mClinicName.setTypeface(font);
            mDoctorName.setTypeface(font);
            mPrescriptionExpiryDate.setTypeface(font);
            mPrescriptionDate.setTypeface(font);


            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color

            int color1 = generator.getColor("FFFFFF");
            // generate color based on a key (same key returns the same color), useful for list/grid views

            // declare the builder object once.
            TextDrawable.IBuilder builder = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .roundRect(2);
            String firstLetter = prescription.getClinicName().substring(0, 1);
            // reuse <></>he builder specs to create multiple drawables
            TextDrawable ic1 = builder.build(firstLetter, color1);
            FirstLetterView.setImageDrawable(ic1);


        }

        @Override
        public void onClick(View view) {
            /* to navigate to next activity along with the Prescription id */

        }
        /*
               Format date for the given date object and returns formatted date in String form
         */
        private String formatDate(Date date) {
            DateFormat df = new SimpleDateFormat("dd/MM/yy");
            Date dateToConvert = date;
            String convertedDateString = null;
            try {
                convertedDateString = df.format(dateToConvert);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertedDateString;


        }


    }
        /*
            Whenever the screen Prescription List screen loads , it has to be updated with recent changes
         */
    @Override
    public void onResume() {
        super.onResume();
        mPrescriptionAdapter.setPrescriptionList(PrescriptionManager.get(getActivity()).getPrescriptionsForProfile(Integer.parseInt(getActivity().getIntent().getStringExtra("profile_id"))));
        mPrescriptionAdapter.notifyDataSetChanged();
         }
}
