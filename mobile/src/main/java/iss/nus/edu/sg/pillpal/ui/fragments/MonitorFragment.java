package iss.nus.edu.sg.pillpal.ui.fragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;

/**
 * Created by Samrat on 27/8/16.
 */
public class MonitorFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    /****************************************************/
    // Instance variables
    /****************************************************/
    String mHighTimeStamp;
    String mHighValue;
    String mCurrentValue;
    String mLowValue;
    String mLowTimeStamp;
    String mCurrentTime;

    @Bind(R.id.current_Heart_Rate)
    TextView mCurrentHeartRate;
    @Bind(R.id.highest_heart_rate)
    TextView mHighestHeartRate;
    @Bind(R.id.lowest_haert_rate)
    TextView mLowestHeartRate;
    @Bind(R.id.current_Heart_Rate_Time_Stamp)
    TextView mCurrentTimeStamp;
    @Bind(R.id.highest_Heart_Rate_time_stamp)
    TextView mHighestTimeStamp;
    @Bind(R.id.lowest_heart_rate_time_stamp)
    TextView mLowestTimeStamp;
    @Bind(R.id.marqueeText)
    TextView mMarqueeText;

    /****************************************************/
    // Constructor

    /****************************************************/
    public MonitorFragment() {
        // Required empty public constructor
    }

    /****************************************************/
    // View Lifecycle

    /****************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_monitor_recreated, container, false);
        // Butterknife bind
        ButterKnife.bind(this, view);
        mMarqueeText.setSelected(true);
        // Return the view

        return view;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
        readSharePreferencesAndUpdateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences preferences = getActivity().getSharedPreferences("MONITORING", 0);
        preferences.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        SharedPreferences preferences = getActivity().getSharedPreferences("MONITORING", 0);
        preferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }
    /****************************************************/
    // SharedPreferences.OnSharedPreferenceChangeListener
    /****************************************************/
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        readSharePreferencesAndUpdateUI();
    }

    /****************************************************/
    // Helper Methods
    /****************************************************/
    public void readSharePreferencesAndUpdateUI(){
        SharedPreferences preferences = getActivity().getSharedPreferences("MONITORING", 0);
        mHighTimeStamp = preferences.getString("HighTimeStamp","01/01/2999 12:12:12");
        mLowTimeStamp = preferences.getString("LowTimeStamp","01/01/2999 12:12:12");
        mCurrentTime = preferences.getString("CurrentTimeStamp","01/01/2999 12:12:12");
        mCurrentValue = preferences.getString("CurrentValue","073");
        mLowValue = preferences.getString("LowValue","034");
        mHighValue = preferences.getString("HighValue","141");
        heartRateValueSet();
    }
    public void heartRateValueSet(){
        mCurrentHeartRate.setText(mCurrentValue);
        mHighestHeartRate.setText(mHighValue);
        mLowestHeartRate.setText(mLowValue);
        mCurrentTimeStamp.setText(mCurrentTime);
        mHighestTimeStamp.setText(mHighTimeStamp);
        mLowestTimeStamp.setText(mLowTimeStamp);
        changeHeartRateIndication();
    }

    public void changeHeartRateIndication(){

        if(Integer.parseInt(mCurrentValue) >= 85 || Integer.parseInt(mCurrentValue) < 60) {
            mCurrentHeartRate.setTextColor(Color.parseColor("#B93301"));
        } else {
            mCurrentHeartRate.setTextColor(Color.parseColor("#0C9E53"));
        }
        if(Integer.parseInt(mHighValue) >= 85 || Integer.parseInt(mHighValue) < 60) {
            mHighestHeartRate.setTextColor(Color.parseColor("#B93301"));
        } else {
            mHighestHeartRate.setTextColor(Color.parseColor("#0C9E53"));
        }
        if(Integer.parseInt(mLowValue) > 85 || Integer.parseInt(mLowValue) < 60) {
            mLowestHeartRate.setTextColor(Color.parseColor("#B93301"));
        } else {
            mLowestHeartRate.setTextColor(Color.parseColor("#0C9E53"));
        }

    }
}
