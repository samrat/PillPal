package iss.nus.edu.sg.pillpal.models;

import java.util.ArrayList;
import java.util.Date;

public class Prescription {

    /**************************************************/
    // Instance Variables
    /**************************************************/
    /**
     * The id of the prescription
     */
    private int mId;

    /**
     * The profile for which the prescription has been issued.
     */
    private Profile mProfile;
    /**
     * The doctor who issued the prescription.
     */
    private String mDoctorName;
    /**
     * The issue date for the prescription.
     */
    /**
     * Clinic Name of the Prescription
     */
    private  String mClinicName;

/*
    Date of Prescription
 */

    private Date mPrescriptionDate;
    /*
           Prescription Expiry Date
     */

    private Date mPrescriptionExpiryDate;



    /**
     * The validity of this prescription (from the issue date).
     */
    /**
     * The list of prescription items that are part of this prescription.
     */

    private ArrayList<PrescriptionItem> mPrescriptionItems ;

    /**************************************************/
    // Constructors
    /**************************************************/
    /**
     * Default constructor
     */
    public Prescription() {
        mPrescriptionItems = new ArrayList<>();
    }

    /**
     * Constructor
     * @param mId The id of the prescription
     * @param mProfile The profile for which the prescription is issued.
     * @param mPrescriptionItems The list of items in the prescription.
     */
    public Prescription(int mId, Profile mProfile, ArrayList<PrescriptionItem> mPrescriptionItems) {
        this.mId = mId;
        this.mProfile = mProfile;
        this.mPrescriptionItems = mPrescriptionItems;
    }

    public Prescription(int id, Profile profile, String doctorName, String clinicName, Date prescriptionDate, Date prescriptionExpiryDate, ArrayList<PrescriptionItem> prescriptionItems) {
        mId = id;
        mProfile = profile;
        mDoctorName = doctorName;
        mClinicName = clinicName;
        mPrescriptionDate = prescriptionDate;
        mPrescriptionExpiryDate = prescriptionExpiryDate;
        mPrescriptionItems = prescriptionItems;
    }

    /**************************************************/
    // Setters & getters
    /**************************************************/
    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public Profile getProfile() {
        return mProfile;
    }

    public void setProfile(Profile profile) {
        this.mProfile = profile;
    }

    public ArrayList<PrescriptionItem> getPrescriptionItems() {
        return mPrescriptionItems;
    }

    public void setPrescriptionItems(ArrayList<PrescriptionItem> prescriptionItems) {
        this.mPrescriptionItems = prescriptionItems;
    }

    public String getDoctorName() {
        return mDoctorName;
    }

    public void setDoctorName(String doctorName) {
        mDoctorName = doctorName;
    }

    public Date getPrescriptionDate() {
        return mPrescriptionDate;
    }

    public void setPrescriptionDate(Date prescriptionDate) {
        mPrescriptionDate = prescriptionDate;
    }

    public String getClinicName() {
        return mClinicName;
    }

    public void setClinicName(String mClinicName) {
        this.mClinicName = mClinicName;
    }
    public Date getPrescriptionExpiryDate() {
        return mPrescriptionExpiryDate;
    }

    public void setPrescriptionExpiryDate(Date mPrescriptionExpiryDate) {
        this.mPrescriptionExpiryDate = mPrescriptionExpiryDate;
    }
}
