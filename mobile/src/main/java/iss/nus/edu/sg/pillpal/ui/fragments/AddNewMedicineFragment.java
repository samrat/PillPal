package iss.nus.edu.sg.pillpal.ui.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.ui.activities.AddMedicine;
import iss.nus.edu.sg.pillpal.utilities.ImageRecogniserUtility;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;
import iss.nus.edu.sg.pillpal.utilities.PictureUtility;
import iss.nus.edu.sg.pillpal.utilities.UIUtils;
import iss.nus.edu.sg.pillpal.utilities.VoiceUtility;

/**
 * @author Bavithra Thangaraj on 9/1/2016.
 */
public class AddNewMedicineFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    @Bind(R.id.edit_name)
    EditText mEditName;
    @Bind(R.id.edit_details)
    EditText mEditDetails;
    @Bind(R.id.edit_expiry)
    EditText mExpDate;
    @Bind(R.id.edit_quantity)
    EditText mEditQuantity;
    @Bind(R.id.edit_qtythreshold)
    EditText mQtyThreshold;
    @Bind(R.id.edit_expthreshold)
    EditText mExpThreshold;
    @Bind(R.id.btn_voice)
    ImageView mVoice;
    @Bind(R.id.btn_calender)
    ImageView mCalender;
    @Bind(R.id.plus_qty_threshold)
    ImageView mPlusQtyThreshold;
    @Bind(R.id.minus_qty_threshold)
    ImageView mMinusQtyThreshold;
    @Bind(R.id.plus_exp_threshold)
    ImageView mPlusExpThreshold;
    @Bind(R.id.minus_exp_threshold)
    ImageView mMinusExpThreshold;
    @Bind(R.id.btn_add_pill)
    Button mAddPill;
    @Bind(R.id.btn_reset)
    Button mReset;
    @Bind(R.id.add_batch_Button)
    FloatingActionButton mAddBatch;
    @Bind(R.id.medicine_unit)
    Spinner mSpinner;

    private ArrayList<String> mVisionResults;
    private MaterialDialog dialog;
    private String mUnit;
    private EditText mEditNamePrompt;
    private EditText mEditDetailsPrompt;
    private ListView mVisionOutputList;
    private AlertDialog.Builder alertDialogBuilder;

    public AddNewMedicineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_new_medicine, container, false);
        ButterKnife.bind(this, rootView);

        this.mExpDate.setOnClickListener(this);
        this.mVoice.setOnClickListener(this);
        this.mCalender.setOnClickListener(this);
        this.mAddPill.setOnClickListener(this);
        this.mReset.setOnClickListener(this);
        this.mPlusQtyThreshold.setOnClickListener(this);
        this.mMinusQtyThreshold.setOnClickListener(this);
        this.mPlusExpThreshold.setOnClickListener(this);
        this.mMinusExpThreshold.setOnClickListener(this);
        this.mAddBatch.setOnClickListener(this);
        this.mSpinner.setOnItemSelectedListener(this);


        if (getActivity().getIntent().getExtras() != null && !getActivity().getIntent().getExtras().containsKey("PILL_ID")) {

            checkIfWeNeedToCallCloudVision();
        }

        return rootView;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }

    public void resultAlertDialog() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.medicine_camera_result, null);
        alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);

        mVisionOutputList = (ListView) promptsView.findViewById(R.id.vision_output);
        mEditNamePrompt = (EditText) promptsView.findViewById(R.id.input_clinicName);
        mEditDetailsPrompt = (EditText) promptsView.findViewById(R.id.input_doctorName);

        mVisionOutputList.setAdapter(null);
        if (mVisionOutputList.getAdapter() == null) {
            //adding Material Design Progress Dialog
            dialog = new MaterialDialog.Builder(getContext())
                    .title(R.string.progress_dialog)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .show();
        }

    }

    private  void resultSuccess() {
        mVisionOutputList.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mVisionResults));
        dialog.dismiss();
        setFields(mVisionOutputList);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mEditName.setText(mEditNamePrompt.getText());
                                mEditDetails.setText(mEditDetailsPrompt.getText());
                                mVisionOutputList.setAdapter(null);
                                mVisionResults = null;
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mVisionOutputList.setAdapter(null);
                                mVisionResults = null;
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    /**
     * Method check if we need to call cloud vision.
     */
    public void checkIfWeNeedToCallCloudVision() {

        ImageRecogniserUtility imageRecogniserUtility = new ImageRecogniserUtility();
        resultAlertDialog();
        Uri uri = PictureUtility.getCameraFileUri();
        if(getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(AddMedicine.PICTURE_PATH)) {
            uri = (Uri) getActivity().getIntent().getExtras().get(AddMedicine.PICTURE_PATH);
        }

        imageRecogniserUtility.recogniseTextInImage(uri, getActivity().getContentResolver(), new ImageRecogniserUtility.OnImageRecognition() {
            @Override
            public void onSuccess(ArrayList<String> texts) {
                mVisionResults = texts;
                resultSuccess();
            }
            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), "Fail to process the image", Toast.LENGTH_LONG).show();
//                dialog.dismiss();
            }

        });

    }

    private void setFields(ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (mEditNamePrompt.hasFocus()) {
                    mEditNamePrompt.setText(selectedItem);
                } else if (mEditDetailsPrompt.hasFocus()) {
                    mEditDetailsPrompt.setText(selectedItem);
                } else {
                    Toast.makeText(getContext(), "NO FIELD SELECTED", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override

    public void onAttach(Context context) {
        super.onAttach(context);
    }


    //gets the code and provide respective result
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == 1010 && resultCode == Activity.RESULT_OK) {
//            this.mVisionOutputLayout.setVisibility(View.VISIBLE);
            this.mVisionResults = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//            this.mVisionOutputList.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mVisionResults));
//            this.setFields(this.mVisionOutputList);
            resultAlertDialog();
            resultSuccess();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    /****************************************************/
    // View onClick

    /****************************************************/
    @Override
    public void onClick(View view) {
        int nQtyCount = 0, nQtyThresholdCount = 0, nExpThresholdCount = 0;
        int id = view.getId();
        switch (id) {
            case R.id.btn_voice:
                if (UIUtils.isNetworkAvailable(AddNewMedicineFragment.this)) {
                    VoiceUtility.getVoiceInput(AddNewMedicineFragment.this);
                } else {
                    Toast.makeText(getContext(), "This function Requires Internet. Please Turn it on", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_calender:
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                UIUtils.selectCalender(AddNewMedicineFragment.this, mExpDate);
                break;

            case (R.id.plus_qty_threshold):
                UIUtils.setPlusQuantity(mQtyThreshold, nQtyThresholdCount);
                break;

            case (R.id.minus_qty_threshold):
                UIUtils.setMinusQuantity(mQtyThreshold, nQtyThresholdCount);
                break;

            case (R.id.plus_exp_threshold):
                UIUtils.setPlusQuantity(mExpThreshold, nExpThresholdCount);
                break;

            case (R.id.minus_exp_threshold):
                UIUtils.setMinusQuantity(mExpThreshold, nExpThresholdCount);
                break;

            case (R.id.btn_add_pill):
                //checking for empty fields while adding
                if (mEditName.getText().toString().trim().length() > 0 &&
                        mEditDetails.getText().toString().trim().length() > 0 &&
                        mExpDate.getText().toString().trim().length() > 0 &&
                        mEditQuantity.getText().toString().trim().length() > 0) {
                    try {
                        this.addPill();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_reset:
                reset();
                break;
        }
    }


    //Adding Pill to the Inventory
    private void addPill() throws ParseException {
        final Pill pill = new Pill();
        pill.setName(mEditName.getText().toString());
        pill.setDetails(mEditDetails.getText().toString());
        try {
            pill.setExpiryDate(UIUtils.simpleDateFormat.parse(mExpDate.getText().toString()));
            pill.setQuantity(Integer.parseInt(mEditQuantity.getText().toString()));
            pill.setThresholdQuantity(Integer.parseInt(mQtyThreshold.getText().toString()));
            pill.setNumberOfDaysThresholdForExpiry(Integer.parseInt(mExpThreshold.getText().toString()));
            pill.setUnit(mUnit);

                              } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(getContext(), "Please Enter valid values", Toast.LENGTH_SHORT).show();
            return;
        }
        InventoryManager iInventoryManager = InventoryManager.get(getActivity());
        if (iInventoryManager.addPill(pill) != null) {
            Toast.makeText(getContext(), "Added Successfully", Toast.LENGTH_SHORT).show();
            reset();
            Intent output = new Intent();
            output.putExtra("pill",  pill.getName());
            getActivity().setResult(1, output);
        }
        else {
            Toast.makeText(getContext(), "This medicine already exist in the inventory", Toast.LENGTH_SHORT).show();
        }
        Log.i("Pill", pill.toString());
    }

    private String selectUnit() {

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                       int position, long id) {
                mUnit = parentView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        return mUnit;
    }

    //Resetting all the filled values
    private void reset() {
        this.mEditName.setText("");
        this.mEditDetails.setText("");
        this.mEditQuantity.setText("");
        this.mQtyThreshold.setText("");
        this.mExpDate.setText("");
        this.mExpThreshold.setText("");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mUnit = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
