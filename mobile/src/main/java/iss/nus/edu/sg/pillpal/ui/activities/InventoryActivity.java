package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.InventoryFragment;


public class InventoryActivity extends BaseSingleFragmentActivity {

    /****************************************************/
    // Abstract Method Implementation
    /****************************************************/

    protected Fragment createFragment() {
        return new InventoryFragment();
    }
}

