package iss.nus.edu.sg.pillpal.ui.activities;

import android.support.v4.app.Fragment;

import iss.nus.edu.sg.pillpal.ui.fragments.CreatePrescriptionFragment;
import iss.nus.edu.sg.pillpal.ui.fragments.PrescriptionListFragment;
import iss.nus.edu.sg.pillpal.ui.fragments.ProfileFragment;
import iss.nus.edu.sg.pillpal.ui.fragments.ViewPrescriptionFragment;

public class PrescriptionActivity extends BaseSingleFragmentActivity {

    protected Fragment createFragment() {

        if (!(getIntent().getStringExtra("Profile") == null) && getIntent().getStringExtra("Profile").equals("Profile")) {
            return new PrescriptionListFragment();
        } else if (!(getIntent().getStringExtra("PrescriptionList") == null) && getIntent().getStringExtra("PrescriptionList").equals("PrescriptionList")) {
            return new ViewPrescriptionFragment();
        } else if (getIntent().getExtras().containsKey("profile_id")) {
            return new CreatePrescriptionFragment();
        }
////        else if (!(getIntent().getStringExtra("ViewPrescriptionFragment") == null) && getIntent().getStringExtra("ViewPrescriptionFragment").equals("ViewPrescriptionFragment")) {
////            return new ViewPrescriptionFragment();
//        }
//         else if (!(getIntent().getStringExtra("ViewPrescriptionFragment") == null)) {
//            return new ViewPrescriptionFragment();
//        }

        return new ProfileFragment();


    }
}
