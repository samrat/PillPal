package iss.nus.edu.sg.pillpal;

import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.test.AndroidTestCase;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class InventoryManagerTesting extends AndroidTestCase {

	private static InventoryManager manager;

    @Override
    protected void setUp() throws Exception {
    	super.setUp();
    	manager = InventoryManager.get(mContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        manager = null;
    }
    
    public void testAddPill(){
    	Pill pill = new Pill();
    	pill.setBatches(null);
    	pill.setDetails("detail");
    	pill.setExpired(false);
    	pill.setExpiryDate(new Date());
    	pill.setName(UUID.randomUUID().toString());
    	pill.setNumberOfDaysThresholdForExpiry(9);
    	pill.setOutOfStock(false);
    	pill.setQuantity(100);
    	pill.setThresholdQuantity(20);
    	pill = manager.addPill(pill);
        assertTrue(pill.getId() > 0);
    }
    
    public void testGetAllPills(){
    	Pill pill = new Pill();
    	pill.setBatches(null);
    	pill.setDetails("detail");
    	pill.setExpired(false);
    	pill.setExpiryDate(new Date());
    	pill.setName(UUID.randomUUID().toString());
    	pill.setNumberOfDaysThresholdForExpiry(9);
    	pill.setOutOfStock(false);
    	pill.setQuantity(100);
    	pill.setThresholdQuantity(20);
    	pill = manager.addPill(pill);
    	List<Pill> lst = manager.getAllPills();
    	assertTrue(lst.size() == 1);
    }
    
    public void testDeleteBatch() {
    	List<Pill> lst = manager.getAllPills();
    	for (Pill pill: lst) {
    		ArrayList<Batch> arr = pill.getBatches();
    		for (Batch batch: arr) {
    			manager.deleteBatch(batch.getId());
    		}
    	}
    	assertTrue(manager.getAllPills().size() == 0);
    }
    
    public void testGetPill(){
    	Pill pill = new Pill();
    	pill.setBatches(null);
    	pill.setDetails("detail");
    	pill.setExpired(false);
    	pill.setExpiryDate(new Date());
    	pill.setName(UUID.randomUUID().toString());
    	pill.setNumberOfDaysThresholdForExpiry(9);
    	pill.setOutOfStock(false);
    	pill.setQuantity(100);
    	pill.setThresholdQuantity(20);
    	Pill pill2 = manager.addPill(pill);
    	
        assertTrue(pill2 != null);
        assertTrue(pill.getQuantity() == pill2.getQuantity());
    }
}
