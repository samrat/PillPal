package iss.nus.edu.sg.pillpal.data;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import iss.nus.edu.sg.pillpal.data.PillPalDbSchema.BatchTable;
import iss.nus.edu.sg.pillpal.data.PillPalDbSchema.PillTable;
import iss.nus.edu.sg.pillpal.interfaces.IInventoryManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;

/**
 * Created by Samrat on 13/5/16.
 */
public class InventoryManager implements IInventoryManager {

    /**************************************************/
    // Constants
    /**************************************************/
    private static final String TAG = "InventoryManager";

    /**************************************************/
    // Instance Variables
    /**************************************************/
    private Context mContext;
    private PillPalBaseHelper mPillPalBaseHelper;

    private static InventoryManager sInventoryManager;
    /**************************************************/
    // Constructor

    /**************************************************/
    private InventoryManager(Context context) {
        mContext = context.getApplicationContext();
        mPillPalBaseHelper = new PillPalBaseHelper(mContext);
        try {
            mPillPalBaseHelper.createDB();
        } catch (Exception e) {
            Log.d(TAG, "DB not created");
        }
    }

    /**************************************************/
    // Public Methods
    /**************************************************/

    /**
     * Returns the singleton instance of the Inventory Manager
     *
     * @param context The current context of the caller
     * @return The instance of the Inventory Manager
     */
    public static InventoryManager get(Context context) {
        if (sInventoryManager == null) {
            sInventoryManager = new InventoryManager(context);
        }
        return sInventoryManager;
    }

    /**
     * Method to get all pills that are present in the database.
     * This takes into account all the batches of the medicine.
     * It sets the various flags(out of stock, expired, etc) also before returning.
     *
     * @return The list of all pills. If the dB is empty, then an empty list is returned.
     */
    public List<Pill> getAllPills() {
        List<Pill> pillList = new ArrayList<>();
        Cursor cursor = null;

        try {
            mPillPalBaseHelper.openDB();
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery("SELECT * FROM " + PillTable.NAME, null);
            try {
                while (cursor.moveToNext()) {
                    Pill pill = getPill(cursor);
                    pillList.add(pill);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getAllPills");
                e.printStackTrace();
            }

            // Loop the list of pill now & get the batches for each
            for (Pill pill : pillList) {
                cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(getBatchForPillIdQuery(pill), null);
                try {
                    while (cursor.moveToNext()) {
                        Batch batch = getBatch(cursor);
                        pill.getBatches().add(batch);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception while fetching getAllPills");
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getAllPills");
            e.printStackTrace();
        } finally {
            closeDBHelperAndCursor(cursor);
        }
        // Update the values & return
        pillList = updatePillList(pillList);

        // Sort the pill list
        Collections.sort(pillList, new Comparator<Pill>() {
            public int compare(Pill firstPill, Pill secondPill) {
                return firstPill.getName().compareToIgnoreCase(secondPill.getName());
            }
        });

        // Return the sorted list
        return pillList;
    }

    /**
     * Method to add a new pill to the database.
     * This add the pill details & creates the first batch.
     * All the required parameters of the pill need to be passed before successful addition takes place.
     * @param pill The pill that needs to be added to the dB.
     * @return If successful, the pill object that was added to the database, else null.
     */
    public Pill addPill(Pill pill) {
        if (validatePill(pill)) {
            if (!checkIfPillExists(pill)) {
                Cursor cursor = null;
                try {
                    // Add in Pill table & also the batch table.
                    String insertQueryForPill = getInsertQueryForPill(pill);
                    // Fire the insert
                    mPillPalBaseHelper.getWritableDatabase().execSQL(insertQueryForPill);
                    // Time to retrieve the last row
                    cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery("SELECT last_insert_rowid();", null);
                    int pillId = -1;
                    try {
                        while (cursor.moveToNext()) {
                            pillId = cursor.getInt(0);
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "Exception while getting last added pillId");
                        e.printStackTrace();
                    }

                    if (pillId != -1) { // Successfully inserted.
                        pill.setId(pillId);
                        // Now update the batch for this pill
                        String insertQueryForBatch = getInsertQueryForBatch(pill);
                        // Fire the insert
                        mPillPalBaseHelper.getWritableDatabase().execSQL(insertQueryForBatch);
                        // Get the updated pill & return
                        return getPill(pill.getId());
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception while addPill");
                    e.printStackTrace();
                } finally {
                    closeDBHelperAndCursor(cursor);
                }
            }
        }
        return null;
    }

    /**
     * Method to update the threshold values of the pill.
     *
     * @param pillId            The pill id for which the attributes need to be upgraded.
     * @param thresholdQuantity The threshold quantity that needs to be set for the pill.
     * @param thresholdExpiry   The threshold expiry that needs to be set for the pill.
     * @return The updated pill if the update was successful, else null.
     */
    public Pill updateThresholdValuesOfPill(int pillId, int thresholdQuantity, int thresholdExpiry) {
        Pill pill = getPill(pillId);
        if (pill != null) {
            try {
                mPillPalBaseHelper.openDB();
                // Get the query
                String updateQueryForBatch = getUpdateQueryForPill(pillId, thresholdQuantity, thresholdExpiry);
                // Fire the insert
                mPillPalBaseHelper.getWritableDatabase().execSQL(updateQueryForBatch);
                // Return the updated pill
                return getPill(pillId);
            } catch (Exception e) {
                Log.d(TAG, "Exception while addBatch");
                e.printStackTrace();
            } finally {
                mPillPalBaseHelper.close();
            }
        }
        return null;
    }

    /**
     * Method to update the pill when the user consumes the pill.
     * @param pillId The pill that needs to be updated.
     * @param quantity The quantity that the user has consumed.
     * @return The updated pill if successfully added, else null.
     */
    public Pill updatePillForConsumption(int pillId, int quantity) {
        Pill pill = getPill(pillId);
        if(pill != null) {

            Collections.sort(pill.getBatches(), new Comparator<Batch>() {
                public int compare(Batch firstBatch, Batch secondBatch) {
                    return firstBatch.getExpiryDate().compareTo(secondBatch.getExpiryDate());
                }
            });

            /*
            The below logic should be a recursive function, but we did not have time to write & test the same.
            Sorry to the guys who are taking up this project.
            * */
            int count = pill.getBatches().size();
            Batch batch = pill.getBatches().get(count - 1);

            if(batch.getQuantity() > quantity) {
                batch.setQuantity(batch.getQuantity() - quantity);
                updateExistingBatch(batch);
            }else if(batch.getQuantity() == quantity) {
                deleteBatch(batch.getId());
            }else {
                // Ideally this sho
                int residue = quantity - batch.getQuantity();
                deleteBatch(batch.getId());
                Batch batch1 = pill.getBatches().get(count - 2);
                batch1.setQuantity(batch.getQuantity() - quantity);
                updateExistingBatch(batch1);
            }

            // First get the latest batch
            // Next check if that batch has the quantity that was consumed.
                // If equal, then just delete the batch
                // If greater, then update the quantity
                // If less, then delete the batch & also remove the rest from the next batch.
            // Return the updated pill
        }
        return null;
    }

    /**
     * Method to add a batch to an existing pill.
     *
     * @param pillId The pill Id that the batch needs to be added to.
     * @param batch  The batch object that needs to be added to the pill.
     * @return The updated pill if successfully added, else null.
     */
    public Pill addBatch(int pillId, Batch batch) {
        Pill pill = getPill(pillId);
        if (validateBatch(batch) && pill != null) {
            Batch existingBatch = getExistingBatchWithSameExpiryDate(pill, batch);
            /* If the a batch with the same date is already present, then it is added to the existing batch. */
            if (existingBatch != null) {
                existingBatch.setQuantity(existingBatch.getQuantity() + batch.getQuantity());
                return updateExistingBatch(existingBatch);
            } else {
                return addNewBatch(pillId, batch);
            }
        }
        return null;
    }

    /**
     * Method to delete a batch associated with a pill.
     * If it is the last batch associated with the pill, then the pill is also deleted.
     *
     * @param batchId The batch id that needs to be deleted.
     */
    public boolean deleteBatch(int batchId) {

        Cursor cursor = null;
        Batch batch = null;
        int deletedRows = 0;
        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + BatchTable.NAME + " WHERE " + BatchTable.Cols.ID + " = " + batchId;
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            // First get the pill
            try {
                while (cursor.moveToNext()) {
                    batch = getBatch(cursor);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getPill from dB");
                e.printStackTrace();
            }
            // Get the pill now
            Pill pill = getPill(batch.getPillId());
            if (pill.getBatches().size() == 1) {
                // Delete the pill
                mPillPalBaseHelper.getWritableDatabase().delete(PillTable.NAME, "id = " + pill.getId(), null);
            }
            // Delete the batch no matter what
            deletedRows = mPillPalBaseHelper.getWritableDatabase().delete(BatchTable.NAME, "id = " + batchId, null);

        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getPill from dB");
            e.printStackTrace();
        } finally {
            closeDBHelperAndCursor(cursor);
        }

        // Confirm if the row was successfully deleted
        if (deletedRows == 1) {
            return true;
        }
        return false;
    }

    /**
     * Method to get a particular based on its Id.
     *
     * @param pillId The pill id for which the pill needs to be queried.
     * @return If the id is present in dB the pill, else null.
     */
    public Pill getPill(int pillId) {
        Cursor cursor = null;
        Pill pill = null;
        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + PillTable.NAME + " WHERE " + PillTable.Cols.ID + " = " + pillId;
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            // First get the pill
            try {
                while (cursor.moveToNext()) {
                    pill = getPill(cursor);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getPill from dB");
                e.printStackTrace();
            }

            // Now get the batch
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(getBatchForPillIdQuery(pill), null);
            try {
                while (cursor.moveToNext()) {
                    Batch batch = getBatch(cursor);
                    pill.getBatches().add(batch);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getAllPills");
                e.printStackTrace();
            }

        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getPill from dB");
            e.printStackTrace();
        } finally {
            closeDBHelperAndCursor(cursor);
        }
        return updatePillParametersBasedOnBatches(pill);
    }

    /**
     * Method to get a particular based on its name.
     *
     * @param pillName The pill name for which the pill needs to be queried.
     * @return If the name is present in dB the pill, else null.
     */
    public Pill getPillByName(String pillName) {
        Cursor cursor = null;
        Pill pill = null;
        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + PillTable.NAME + " WHERE " + PillTable.Cols.NAME + " = '" + pillName + "'";
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            // First get the pill
            try {
                while (cursor.moveToNext()) {
                    pill = getPill(cursor);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getPill from dB");
                e.printStackTrace();
            }

            // Now get the batch
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(getBatchForPillIdQuery(pill), null);
            try {
                while (cursor.moveToNext()) {
                    Batch batch = getBatch(cursor);
                    pill.getBatches().add(batch);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getAllPills");
                e.printStackTrace();
            }

        } catch (Exception e) {
            Log.d(TAG, "Exception while fetching getPill from dB");
            e.printStackTrace();
        } finally {
            closeDBHelperAndCursor(cursor);
        }
        return updatePillParametersBasedOnBatches(pill);
    }

    /**
     * Method to get only the expired medicines.
     *
     * @return The list of expired medicines.
     */
    public List<Pill> getExpiredMedicines() {
        List<Pill> pillList = new ArrayList<>();
        for (Pill pill : getAllPills()) {
            if (pill.isExpired()) {
                pillList.add(pill);
            }
        }

        // Now sort based on the expiry date

        // Sort the pill list based on the expiry date
        Collections.sort(pillList, new Comparator<Pill>() {
            public int compare(Pill firstPill, Pill secondPill) {
                return firstPill.getExpiryDate().compareTo(secondPill.getExpiryDate());
            }
        });

        return pillList;
    }

    /**
     * Method to get out of stock medicines.
     *
     * @return The list of medicines that are out of stock.
     */
    public List<Pill> getOutOfStock() {
        List<Pill> pillList = new ArrayList<>();
        for (Pill pill : getAllPills()) {
            if (pill.isOutOfStock()) {
                pillList.add(pill);
            }
        }

        // Sort the pill list based on the quantity
        Collections.sort(pillList, new Comparator<Pill>() {
            public int compare(Pill firstPill, Pill secondPill) {
                return firstPill.getQuantity() - secondPill.getQuantity();
            }
        });

        return pillList;
    }

    /**************************************************/
    // Private Methods
    /**************************************************/
    /**
     * Gets a particular pill from the cursor.
     *
     * @param cursor The cursor from which the pill needs to be returned
     * @return The Pill object having all the retrieved values from the cursor.
     * If an attribute is not found, then it remains null.
     */
    private Pill getPill(Cursor cursor) {

        Pill pill = new Pill();
        pill.setId(cursor.getInt(cursor.getColumnIndex(PillTable.Cols.ID)));
        pill.setName(cursor.getString(cursor.getColumnIndex(PillTable.Cols.NAME)));
        pill.setDetails(cursor.getString(cursor.getColumnIndex(PillTable.Cols.DETAILS)));
        pill.setUnit(cursor.getString(cursor.getColumnIndex(PillTable.Cols.UNIT)));
        pill.setNumberOfDaysThresholdForExpiry(cursor.getInt(cursor.getColumnIndex(PillTable.Cols.THRESHOLD_EXPIRY)));
        pill.setThresholdQuantity(cursor.getInt(cursor.getColumnIndex(PillTable.Cols.THRESHOLD_QUANTITY)));
        return pill;
    }

    /**
     * Gets a particular batch from the cursor.
     *
     * @param cursor The cursor from which the batch needs to be returned
     * @return The batch object having all the retrieved values from the cursor.
     * If an attribute is not found, then it remains null.
     */
    private Batch getBatch(Cursor cursor) {
        Batch batch = new Batch();
        batch.setId(cursor.getInt(cursor.getColumnIndex(BatchTable.Cols.ID)));
        batch.setPillId(cursor.getInt(cursor.getColumnIndex(BatchTable.Cols.PILL_ID)));
        batch.setQuantity(cursor.getInt(cursor.getColumnIndex(BatchTable.Cols.QUANTITY)));
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String expiryDate = (cursor.getString(cursor.getColumnIndex(BatchTable.Cols.EXPIRY_DATE)));
            batch.setExpiryDate(dateFormat.parse(expiryDate));
        } catch (Exception e) {
            Log.d(TAG, "Exception due to date parsing from db");
            e.printStackTrace();
        }
        return batch;
    }

    /**
     * Method to create a select query with where clause for pill Id.
     *
     * @param pill The pill for which the query needs to be created.
     * @return The updated SELECT query.
     */
    private String getBatchForPillIdQuery(Pill pill) {
        return "SELECT * FROM " + BatchTable.NAME + " WHERE " +
                BatchTable.Cols.PILL_ID + " = " +
                +pill.getId() + ";";
    }

    /**
     * Method to create a select query with where clause for name & details.
     *
     * @param pill The pill for which the query needs to be created.
     * @return The updated SELECT query.
     */
    private String getPillForNameAndDetailsCheckQuery(Pill pill) {
        return "SELECT * FROM " + PillTable.NAME + " WHERE " +
                PillTable.Cols.NAME + " = " +
                "'" + pill.getName() + "'" +
                " AND " +
                PillTable.Cols.DETAILS + " = " +
                "'" + pill.getDetails() + "'" + ";";
    }

    /**
     * Method to create the insert query for pill.
     *
     * @param pill The pill for which the query needs to be created.
     * @return The updated INSERT query.
     */
    private String getInsertQueryForPill(Pill pill) {
        return "INSERT INTO " + PillTable.NAME +
                " ('" + PillTable.Cols.NAME + "'," +
                "'" + PillTable.Cols.DETAILS + "'," +
                "'" + PillTable.Cols.UNIT + "'," +
                "'" + PillTable.Cols.THRESHOLD_EXPIRY + "'," +
                "'" + PillTable.Cols.THRESHOLD_QUANTITY + "')" +
                " VALUES(" +
                "'" + pill.getName() + "'," +
                "'" + pill.getDetails() + "'," +
                "'" + pill.getUnit() + "'," +
                pill.getNumberOfDaysThresholdForExpiry() + "," +
                pill.getThresholdQuantity() + ");";
    }

    /**
     * Method to get the insert query for inserting a batch to an existign pill.
     *
     * @param pillId The pillId of the pill that the batch needs to be added to.
     * @param batch  The batch that needs to be added.
     * @return The udpated insert query.
     */
    private String getInsertQueryForBatch(int pillId, Batch batch) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return "INSERT INTO " + BatchTable.NAME +
                " ('" + BatchTable.Cols.PILL_ID + "'," +
                "'" + BatchTable.Cols.EXPIRY_DATE + "'," +
                "'" + BatchTable.Cols.QUANTITY + "')" +
                " VALUES(" +
                pillId + "," +
                "'" + formatter.format(batch.getExpiryDate()) + "'," +
                batch.getQuantity() + ");";
    }

    /**
     * Method to create the insert query for batch.
     *
     * @param pill The pill for which the batch query needs to be created.
     * @return The updated INSERT query.
     */
    private String getInsertQueryForBatch(Pill pill) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return "INSERT INTO " + BatchTable.NAME +
                " ('" + BatchTable.Cols.PILL_ID + "'," +
                "'" + BatchTable.Cols.EXPIRY_DATE + "'," +
                "'" + BatchTable.Cols.QUANTITY + "')" +
                " VALUES(" +
                pill.getId() + "," +
                "'" + formatter.format(pill.getExpiryDate()) + "'," +
                pill.getQuantity() + ");";
    }

    /**
     * Method to get query for updating an existing batch.
     *
     * @param batch The batch for which the query needs to be written
     * @return The udpated query.
     */
    private String getUpdateQueryForBatch(Batch batch) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return "UPDATE " + BatchTable.NAME + " SET " +
                BatchTable.Cols.EXPIRY_DATE + " = " +
                "'" + formatter.format(batch.getExpiryDate()) + "', " +
                BatchTable.Cols.QUANTITY + " = " + batch.getQuantity()
                + " WHERE " + BatchTable.Cols.ID + " = " + batch.getId();
    }

    /**
     * Method to get an update query for the pill. Updates the threshold expiry & threshold quantity.
     *
     * @param pillId            The pill ID that needs to be updated.
     * @param thresholdQuantity The threshold quantity that needs to be set to the pill.
     * @param thresholdExpiry   The threshold expiry that needs to be set to the pill.
     * @return The Update query.
     */
    private String getUpdateQueryForPill(int pillId, int thresholdQuantity, int thresholdExpiry) {
        return "UPDATE " + PillTable.NAME + " SET " +
                PillTable.Cols.THRESHOLD_EXPIRY + " = " + thresholdExpiry
                + ", " + PillTable.Cols.THRESHOLD_QUANTITY + " = " + thresholdQuantity
                + " WHERE " + PillTable.Cols.ID + " = " + pillId;
    }

    /**
     * Method to get an update query for the pill. Updates the threshold expiry & threshold quantity.
     *
     * @param pill The pill from which the values will be extracted.
     * @return The Update query.
     */
    private String getUpdateQueryForPill(Pill pill) {
        return "UPDATE " + PillTable.NAME + " SET " +
                PillTable.Cols.THRESHOLD_EXPIRY + " = " + pill.getNumberOfDaysThresholdForExpiry()
                + ", " + PillTable.Cols.THRESHOLD_QUANTITY + " = " + pill.getThresholdQuantity()
                + " WHERE " + PillTable.Cols.ID + " = " + pill.getId();
    }

    /**
     * Method to update the various pill objects based on their batches.
     * This also updated the "isExpired" of the batches.
     *
     * @param pillList The list containing pills & their respective batches.
     */
    private List<Pill> updatePillList(List<Pill> pillList) {

        for (Pill pill : pillList) {
            updatePillParametersBasedOnBatches(pill);
        }
        return pillList;
    }

    /**
     * Method to update the various pill objects based on their batches.
     * This also updates the "isExpired" of the batches.
     *
     * @param pill The pill containing the respective batches.
     */
    private Pill updatePillParametersBasedOnBatches(Pill pill) {
        /**
         * 1. Update the isExpired of the batch
         * 2. Update the isExpired of the pill
         * 3. Update the isOutOfStock of the pill
         * 4. Update the quantity of the pill
         * 5. Update the expiry date of the pill
         */
        for (Batch batch : pill.getBatches()) {

            // Set the expiry date for the first time
            if (pill.getExpiryDate() == null) {
                pill.setExpiryDate(batch.getExpiryDate());
            }

            // Now check if this is before or not
            if (pill.getExpiryDate().after(batch.getExpiryDate())) {
                pill.setExpiryDate(batch.getExpiryDate());
            }

            // Check if the batch date has expired or not.
            if (new Date().after(batch.getExpiryDate())) {
                batch.setExpired(true);
                pill.setExpired(true);
            }
            // Update the quantity of the pill
            int quantity = pill.getQuantity() + batch.getQuantity();
            pill.setQuantity(quantity);
        }
        // Check if the total quantity if out of stock.
        if (pill.getQuantity() < pill.getThresholdQuantity()) {
            pill.setOutOfStock(true);
        }

        return pill;
    }

    /**
     * Method to validate whether the batch object has all the necessary parameters before adding to the dB.
     * The method checks if there is a "expiry date" is present, the "quantity" > 0 & that the object itself is not null.
     *
     * @param batch The batch that needs to be validated.
     * @return True if the batch is valid, else false.
     */
    private boolean validateBatch(Batch batch) {
        return (batch != null && batch.getExpiryDate() != null && batch.getQuantity() > 0);
    }

    /**
     * Method to check if a pill already has a similar batch with the same expiry date.
     *
     * @param pill  The pill to which the batch needs to be checked against.
     * @param batch The batch that needs to be checked.
     * @return The batch object of the pill that has the same expiry date, else null.
     */
    private Batch getExistingBatchWithSameExpiryDate(Pill pill, Batch batch) {
        Batch returnValue = null;
        if (pill != null && batch != null) {
            Calendar existingCalendar = Calendar.getInstance();
            Calendar newCalendar = Calendar.getInstance();
            newCalendar.setTime(batch.getExpiryDate());

            for (Batch existingBatch : pill.getBatches()) {
                // The two dates need to be same.
                existingCalendar.setTime(existingBatch.getExpiryDate());
                boolean isSameDay = existingCalendar.get(Calendar.YEAR) == newCalendar.get(Calendar.YEAR) &&
                        existingCalendar.get(Calendar.DAY_OF_YEAR) == newCalendar.get(Calendar.DAY_OF_YEAR);
                if (isSameDay) {
                    returnValue = existingBatch;
                    break;
                }
            }
        }
        return returnValue;
    }

    /**
     * Method to update an existing batch.
     *
     * @param batch The batch that needs to be updated.
     * @return If successful, The updated Pill to which the Batch is associated with.
     * Else, null
     */
    private Pill updateExistingBatch(Batch batch) {
        /* Update the existing batch. */
        try {
            mPillPalBaseHelper.openDB();
            // Get the query
            String updateQueryForBatch = getUpdateQueryForBatch(batch);
            // Fire the insert
            mPillPalBaseHelper.getWritableDatabase().execSQL(updateQueryForBatch);
            // Return the updated pill
            return getPill(batch.getPillId());
        } catch (Exception e) {
            Log.d(TAG, "Exception while addBatch");
            e.printStackTrace();
        } finally {
            mPillPalBaseHelper.close();
        }
        return null;
    }

    /**
     * Method to add a new batch to an existing pill.
     *
     * @param pillId The pill to which the batch needs to be added.
     * @param batch  The batch that needs to be added.
     * @return If successful, The updated Pill to which the Batch is associated with.
     * Else, null
     */
    private Pill addNewBatch(int pillId, Batch batch) {
        /* Add a new batch to the pill */
        try {
            mPillPalBaseHelper.openDB();
            // Add the batch
            String insertQueryForBatch = getInsertQueryForBatch(pillId, batch);
            // Fire the insert
            mPillPalBaseHelper.getWritableDatabase().execSQL(insertQueryForBatch);
            // Return the updated pill
            return getPill(pillId);
        } catch (Exception e) {
            Log.d(TAG, "Exception while addBatch");
            e.printStackTrace();
        } finally {
            mPillPalBaseHelper.close();
        }
        return null;
    }

    /**
     * Method to check if the pill(combination of name & details) already exists in database.
     *
     * @param pill The pill that needs to be checked.
     * @return true if it exists, else false.
     */
    private boolean checkIfPillExists(Pill pill) {
        Cursor cursor = null;
        try {
            // First check if the pill already exists!
            mPillPalBaseHelper.openDB();
            String query = getPillForNameAndDetailsCheckQuery(pill);
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            if ((cursor != null) && (cursor.getCount() > 0)) {
                return true;
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception while checkingIfPillExists");
            e.printStackTrace();
        } finally {
            closeDBHelperAndCursor(cursor);
        }
        return false;
    }

    /**
     * Closes the cursor & db helper.
     * @param cursor The curson that needs to be closed.
     */
    private void closeDBHelperAndCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
        mPillPalBaseHelper.close();
    }

    /**
     * Method to validate the pill before adding it to the dB.
     * @param pill The pill that needs to be validated.
     * @return True if all the required parameters are present, else false.
     */
    private boolean validatePill(Pill pill) {
        return ((pill != null) && (pill.getExpiryDate() != null) && (pill.getQuantity() > 0)
                && (pill.getName() != null) && (pill.getDetails() != null) && (pill.getThresholdQuantity() >= 0)
                && (pill.getNumberOfDaysThresholdForExpiry() >= 0));
    }
}


