package iss.nus.edu.sg.pillpal.ui.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import iss.nus.edu.sg.pillpal.R;
import iss.nus.edu.sg.pillpal.adapter.PrescriptionAdapter;
import iss.nus.edu.sg.pillpal.data.InventoryManager;
import iss.nus.edu.sg.pillpal.data.PrescriptionManager;
import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.interfaces.IPrescriptionManager;
import iss.nus.edu.sg.pillpal.models.Batch;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.ui.activities.AddMedicine;
import iss.nus.edu.sg.pillpal.utilities.ImageRecogniserUtility;
import iss.nus.edu.sg.pillpal.utilities.NavigationUtility;
import iss.nus.edu.sg.pillpal.utilities.PictureUtility;
import iss.nus.edu.sg.pillpal.utilities.UIUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePrescriptionFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String myFormat = "dd/MM/yy";
    private EditText mPrescriptionDoctor;
    private EditText mPrescriptionDate;
    private EditText mPrescriptionClinic;
    private ArrayList<PrescriptionItem> mPrescriptionItems;
    private FloatingActionButton mAddPrescriptionPill;
    private ListView mListPills;
    private BaseAdapter mPrescriptionAdapter;
    private Prescription prescription;
    private PrescriptionItem mPrescriptionItem;
    private Button mAddPrescription;
    private Pill mSelectedpill;
    private int pillId;
    private Long itemPosition;
    private InventoryManager inventoryManager;
    private SimpleDateFormat sdf;
    private Date mPrescriptionDate1;
    private ArrayList<String> mResults;
    private MaterialDialog dialog;
    private CheckBox mInventoryCheckbox;
    private Batch mBatch;

    private ImageView mPrescriptionCalendar;
    private ImageView mExpiryCalendar;
    private List<String> sname;

    //Code for Revised Add prescription screen
    private Spinner mMedicineList;
    private EditText mRoutine;
    private Spinner mRoutineTimes;
    private EditText mDosage;
    private Spinner mDosageQuantity;
    private Spinner mEatMedicine;
    private int mRoutineItemPosition = 0;
    private int mDosageItemPostion = 0;
    private int mInstructionItemPosition = 0;
    private EditText mPrescriptionExpiryDate;
    private Date mExpiryDate;
    private ArrayAdapter<String> mMedicineadapter;
    private ArrayAdapter<String> mMedicineadapterUpdated;
    private ArrayList<String> mVisionResults;
    private ListView mVisionOutputList;
    private EditText mDialogClinicName;
    private EditText mDialogDoctorName;
    Profile profile;

    private Date mPrescriptionDateForm;
    private  Date mExpiryPrescriptionDateForm;

    public CreatePrescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_create_prescription, container, false);
        this.mPrescriptionDoctor = (EditText) rootView.findViewById(R.id.input_doctorName);
        this.mPrescriptionDate = (EditText) rootView.findViewById(R.id.edit_prescriptionDate);
        this.mPrescriptionClinic = (EditText) rootView.findViewById(R.id.input_clinicName);
        this.mPrescriptionDate.setOnClickListener(this);
        this.mAddPrescription = (Button) rootView.findViewById(R.id.btn_add);
        this.mPrescriptionDate1 = new Date();
        this.mAddPrescription.setOnClickListener(this);
        this.mAddPrescriptionPill = (FloatingActionButton) rootView.findViewById(R.id.btn_add_prescription_pill);
        this.mAddPrescriptionPill.setOnClickListener(this);
        mPrescriptionItem = new PrescriptionItem();
        mPrescriptionItems = new ArrayList<>();
        sdf = new SimpleDateFormat(myFormat, Locale.US);
        this.mMedicineList = (Spinner) rootView.findViewById(R.id.pill_spinner);

        //code for revised Add prescription screen
        mMedicineList.setOnItemSelectedListener(this);
        this.mRoutine = (EditText) rootView.findViewById(R.id.input_routine);
        this.mRoutineTimes = (Spinner) rootView.findViewById(R.id.routine_spinner);
        mRoutineTimes.setOnItemSelectedListener(this);
        this.mDosage = (EditText) rootView.findViewById(R.id.input_dosage);
        this.mDosageQuantity = (Spinner) rootView.findViewById(R.id.dosage_spinner);
        mDosageQuantity.setOnItemSelectedListener(this);
        this.mEatMedicine = (Spinner) rootView.findViewById(R.id.otherdetail_spinner);
        mEatMedicine.setOnItemSelectedListener(this);
        this.mInventoryCheckbox = (CheckBox) rootView.findViewById(R.id.add_to_inventory);
        this.mPrescriptionExpiryDate = (EditText) rootView.findViewById(R.id.edit_prescriptionExpiryDate);
        this.mExpiryDate = new Date();
        this.mPrescriptionExpiryDate.setOnClickListener(this);
        mPrescriptionDateForm = new Date();
        mExpiryPrescriptionDateForm = new Date();
        mInventoryCheckbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    addBatch();
                }
            }


        });
        mPrescriptionCalendar = (ImageView) rootView.findViewById(R.id.btn_calender_prescription);
        mPrescriptionCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrescriptionDate.setText(UIUtils.selectCalender(CreatePrescriptionFragment.this, mPrescriptionDate));
            }
        });
        mExpiryCalendar = (ImageView) rootView.findViewById(R.id.btn_calender_prescription_expiry);
        mExpiryCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrescriptionExpiryDate.setText(UIUtils.selectCalender(CreatePrescriptionFragment.this, mPrescriptionExpiryDate));

            }
        });

        if (getActivity().getIntent().getExtras().containsKey(AddMedicine.PICTURE_PATH)) {

            checkIfWeNeedToCallCloudVision();
        }

        //profile = new Profile();
        ProfileManager profileManager = ProfileManager.get(getActivity());
        // Crash Fix
        if(getActivity().getIntent().getExtras().containsKey("profile_id")) {
            profile = profileManager.getProfile(Integer.parseInt(getActivity().getIntent().getStringExtra("profile_id")));
        }

        /*
        New Change
         */
        inventoryManager = InventoryManager.get(getActivity());
        List<Pill> profileList = inventoryManager.getAllPills();
        Iterator iter = profileList.iterator();
        sname = new ArrayList<String>();
        while (iter.hasNext()) {
            Pill pill = (Pill) iter.next();
            pillId = pill.getId();
            sname.add(pill.getName());
        }

            /*
             Adapter for Medicine List
             */

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sname);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            /*
            Adapter for Routinetimes
             */

        final ArrayAdapter<CharSequence> adapterRoutineTimes = ArrayAdapter.createFromResource(getActivity(), R.array.Routine, android.R.layout.simple_spinner_dropdown_item);

        // Specify the layout to use when the list of choices appears
        adapterRoutineTimes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mRoutineTimes.setAdapter(adapterRoutineTimes);

        /*
             Adapter for Medicine List
             */
        mMedicineadapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sname);


        mMedicineadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mMedicineList.setAdapter(mMedicineadapter);
        mMedicineadapter.add("New Medicine");
        mMedicineadapter.notifyDataSetChanged();

            /*
            Adapter for DosageQuantity
             */

        final ArrayAdapter<CharSequence> adapterDosageQuantity = ArrayAdapter.createFromResource(getActivity(), R.array.Dosage, android.R.layout.simple_spinner_dropdown_item);

        // Specify the layout to use when the list of choices appears
        adapterDosageQuantity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mDosageQuantity.setAdapter(adapterDosageQuantity);

            /*
            Adapter for EatMedicine
             */

        final ArrayAdapter<CharSequence> adapterEatMedicine = ArrayAdapter.createFromResource(getActivity(), R.array.Other, android.R.layout.simple_spinner_dropdown_item);

        // Specify the layout to use when the list of choices appears
        adapterEatMedicine.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mEatMedicine.setAdapter(adapterEatMedicine);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMedicineadapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sname);
        mMedicineadapter.notifyDataSetChanged();

    }

    private void addBatch() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.add_row, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        final EditText batchExp = (EditText) promptsView.findViewById(R.id.edit_expiry);
        final EditText batchQty = (EditText) promptsView.findViewById(R.id.edit_quantity);
        final ImageView batchCalender = (ImageView) promptsView.findViewById(R.id.btn_calender);
        mBatch = new Batch();

        batchCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                batchExp.setText(UIUtils.selectCalender(CreatePrescriptionFragment.this, batchExp));
            }
        });

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                if (batchQty.getText().toString().trim().length() > 0 &&
                                        batchExp.getText().toString().trim().length() > 0) {
                                    try {
                                        mBatch.setExpiryDate(UIUtils.simpleDateFormat.parse(batchExp.getText().toString()));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (mBatch != null)
                                        mBatch.setQuantity(Integer.parseInt(batchQty.getText().toString()));
                                }
                            }

                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void checkIfWeNeedToCallCloudVision() {

        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.prescription_camera_result, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);

        mVisionOutputList = (ListView) promptsView.findViewById(R.id.vision_output);
        mDialogClinicName = (EditText) promptsView.findViewById(R.id.input_clinicName);
        mDialogDoctorName = (EditText) promptsView.findViewById(R.id.input_doctorName);

        mVisionOutputList.setAdapter(null);
        ImageRecogniserUtility imageRecogniserUtility = new ImageRecogniserUtility();
        if (mVisionOutputList.getAdapter() == null) {
            //adding Material Design Progress Dialog
            dialog = new MaterialDialog.Builder(getContext())
                    .title(R.string.progress_dialog)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .show();
        }

        Uri uri = PictureUtility.getCameraFileUri();
        if(getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(AddMedicine.PICTURE_PATH)) {
            uri = (Uri) getActivity().getIntent().getExtras().get(AddMedicine.PICTURE_PATH);
        }

        imageRecogniserUtility.recogniseTextInImage(uri, getActivity().getContentResolver(), new ImageRecogniserUtility.OnImageRecognition() {
            @Override
            public void onSuccess(ArrayList<String> texts) {
                mVisionResults = texts;
                //mVisionOutputList.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,data));
                mVisionOutputList.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mVisionResults));
                //mVisionOutputLayout.setVisibility(View.VISIBLE);
                dialog.dismiss();
                setFields(mVisionOutputList);
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        mPrescriptionClinic.setText(mDialogClinicName.getText());
                                        mPrescriptionDoctor.setText(mDialogDoctorName.getText());
                                        mVisionOutputList.setAdapter(null);
                                        mVisionResults = null;
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        mVisionOutputList.setAdapter(null);
                                        mVisionResults = null;
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), "Fail to process the image", Toast.LENGTH_LONG).show();
//                dialog.dismiss();
            }

        });

    }

    private void setFields(ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (mDialogClinicName.hasFocus()) {
                    mDialogClinicName.setText(selectedItem);
                } else if (mDialogDoctorName.hasFocus()) {
                    mDialogDoctorName.setText(selectedItem);
                } else {
                    Toast.makeText(getContext(), "NO FIELD SELECTED", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == this.mAddPrescriptionPill) {
            LayoutInflater li = LayoutInflater.from(getContext());
            final View promptsView = li.inflate(R.layout.add_prescription_pill, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setView(promptsView);
            final EditText dosageCount = (EditText) promptsView.findViewById(R.id.input_dosage);
            final EditText quantityAmount = (EditText) promptsView.findViewById(R.id.input_quantity);
            final Spinner pillsList = (Spinner) promptsView.findViewById(R.id.pills);

            inventoryManager = InventoryManager.get(getActivity());
            List<Pill> profileList = inventoryManager.getAllPills();
            Iterator iter = profileList.iterator();
            sname = new ArrayList<String>();
            while (iter.hasNext()) {
                Pill pill = (Pill) iter.next();
                pillId = pill.getId();
                sname.add(pill.getName());
            }
            Log.v("ListSize", Integer.toString(sname.size()));
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sname);
            pillsList.setAdapter(adapter);
            pillsList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    itemPosition = pillsList.getItemIdAtPosition(position);
                    mSelectedpill = inventoryManager.getAllPills().get(itemPosition.intValue());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            pillsList.setAdapter(adapter);
            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Add",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mPrescriptionItem.setPill(mSelectedpill);
                                    mPrescriptionItem.setDosage(dosageCount.getText().toString());
                                    mPrescriptionItem.setRoutine(quantityAmount.getText().toString());
                                    mPrescriptionItems.add(mPrescriptionItem);
                                    mPrescriptionAdapter = new PrescriptionAdapter(CreatePrescriptionFragment.this.getActivity(), getContext(), mPrescriptionItems, mPrescriptionItem);
                                    mListPills.setAdapter(mPrescriptionAdapter);
                                    mPrescriptionAdapter.notifyDataSetChanged();

                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        } else if (v == mAddPrescription) {
            prescription = new Prescription();
            IPrescriptionManager prescriptionManager = PrescriptionManager.get(getActivity());
            setPrescription(prescription);
            if (mSelectedpill != null && mBatch != null) {
                InventoryManager iInventoryManager = InventoryManager.get(getActivity());
                mSelectedpill = iInventoryManager.addBatch(mSelectedpill.getId(), mBatch);
            }

            PrescriptionItem prescriptionItem = setPrescriptionItem();
            mPrescriptionItems.add(prescriptionItem);
            prescription.setPrescriptionItems(mPrescriptionItems);
            prescription.setProfile(profile);
            if (!checkMandatoryPrescriptionFields()) {
                Toast.makeText(getContext(), "Enter all the fields", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                prescriptionManager.addPrescription(prescription);

            } catch (Exception e) {
                e.printStackTrace();
                Log.v("Exception", "Exception while adding Prescription");
            }
            this.reset();
            Toast.makeText(getContext(), "Added sucessfully", Toast.LENGTH_SHORT).show();


        } else if (v == mPrescriptionDate) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
           // mPrescriptionDate.setText(UIUtils.selectCalender(CreatePrescriptionFragment.this, mPrescriptionDate));
           // Log.v("Prescriptiondate",mPrescriptionDate.getText().toString());
            //setPrescriptionDate(mPrescriptionDate.getText().toString());
            selectCalender(mPrescriptionDate);
        } else if (v == mPrescriptionExpiryDate) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
           // mPrescriptionExpiryDate.setText(UIUtils.selectCalender(CreatePrescriptionFragment.this, mPrescriptionExpiryDate));
           // setExpiryPrescriptiondate(mPrescriptionExpiryDate.getText().toString());
            selectCalender(mPrescriptionExpiryDate);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1010 && resultCode == Activity.RESULT_OK) {
            this.mVisionResults = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            this.mVisionOutputList.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mVisionResults));
//            this.setFields(this.mVisionOutputList);
        } else if (requestCode == 1) {
            if (resultCode == 1) {
                String returnedResult = data.getStringExtra("pill");
                List<Pill> profileList = inventoryManager.getAllPills();
                Iterator iter = profileList.iterator();
                mMedicineadapter.add(returnedResult);
                mMedicineList.setAdapter(mMedicineadapter);
                mMedicineadapter.notifyDataSetChanged();

            }
        } else {
            //Toast message for failure
            Toast.makeText(getContext(), "Fail to process the image", Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.pill_spinner:
                if (mMedicineList.getItemAtPosition(position).toString() == "New Medicine") {
                    Intent intent = new Intent(getActivity(), AddMedicine.class);
                    startActivityForResult(intent, 1);
                } else {
                    itemPosition = mMedicineList.getItemIdAtPosition(position);
                    mSelectedpill = inventoryManager.getPillByName(mMedicineadapter.getItem(Integer.parseInt(""+itemPosition)));
                    mPrescriptionItem.setPill(mSelectedpill);
                }

                break;
            case R.id.routine_spinner:
                itemPosition = mRoutineTimes.getItemIdAtPosition(position);
                if (itemPosition == 0) {
                    mRoutineItemPosition = 0;
                } else if (itemPosition == 1) {
                    mRoutineItemPosition = 1;


                }
                break;
            case R.id.dosage_spinner:
                itemPosition = mDosageQuantity.getItemIdAtPosition(position);
                if (itemPosition == 0) {
                    if (!mDosage.getText().equals(null))
                        mDosageItemPostion = 0;
                }
                if (itemPosition == 1) {
                    if (!mDosage.getText().equals(null))
                        mDosageItemPostion = 1;

                }
                break;
            case R.id.otherdetail_spinner:
                itemPosition = mEatMedicine.getItemIdAtPosition(position);
                if (itemPosition == 0) {
                    mInstructionItemPosition = 0;
                } else if (itemPosition == 1) {
                    mInstructionItemPosition = 1;

                }
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        NavigationUtility.addAnimationToHardwareBackButtonForFragment(this);
    }



    /*
        Method to reset all the fields
     */

    private void reset() {
        mPrescriptionDoctor.setText("");
        mPrescriptionClinic.setText("");
        mRoutine.setText("");
        mDosage.setText("");
        mPrescriptionDate.setText("");
        mPrescriptionExpiryDate.setText("");
    }

    /*
        Method to set Prescription Object to be persisted in DB
     */
    private void setPrescription(Prescription prescription) {
        prescription.setDoctorName(mPrescriptionDoctor.getText().toString());
        prescription.setPrescriptionDate(mPrescriptionDate1);
        prescription.setClinicName(mPrescriptionClinic.getText().toString());
        prescription.setPrescriptionExpiryDate(mExpiryDate);
    }

    /*
    Method to set Prescription Items  to be persisted in DB
 */
    private PrescriptionItem setPrescriptionItem() {
        if (mRoutineItemPosition == 0) {
            mPrescriptionItem.setRoutine(mRoutine.getText().toString() + " Times a " + "Day");
        } else if (mRoutineItemPosition == 1) {
            mPrescriptionItem.setRoutine(mRoutine.getText().toString() + " Times a " + "Week");
        }
        if (mDosageItemPostion == 0) {
            mPrescriptionItem.setDosage(mDosage.getText().toString() + " ml");
        } else if (mDosageItemPostion == 1) {
            mPrescriptionItem.setDosage(mDosage.getText().toString() + " Capsule");
        }
        if (mInstructionItemPosition == 0) {
            mPrescriptionItem.setInstructions("Before Lunch");
        } else if (mInstructionItemPosition == 1) {
            mPrescriptionItem.setInstructions("After Lunch");

        }
        return mPrescriptionItem;

    }

    /**
     * @return True of False , checks whether all the fields are entered
     */
    private boolean checkMandatoryPrescriptionFields() {
        boolean isFieldsFilled = true;
        if (mPrescriptionDoctor.getText().toString().equals("") || mPrescriptionClinic.getText().toString().equals("") || mPrescriptionExpiryDate.getText().toString().equals("") ||
                mPrescriptionDate.getText().toString().equals("") || mRoutine.getText().toString().equals("") ||
                mDosage.getText().toString().equals("")) {
            isFieldsFilled = false;
        }
        return isFieldsFilled;
    }

    private void setPrescriptionDate(String date) {
        Log.v("PrescriptionDateBefore",date);
        mPrescriptionDateForm = convertToDate(date);
        Log.v("FormattedDate",mPrescriptionDateForm.toString());
    }

    private void setExpiryPrescriptiondate(String date){
        Log.v("Expirydate",date);
        mExpiryPrescriptionDateForm = convertToDate(date);
        Log.v("FormattedExpirydate",mExpiryPrescriptionDateForm.toString());

    }

    private Date convertToDate(String date) {
        DateFormat df = new SimpleDateFormat(UIUtils.MY_FORMAT);
        Date convertedDate = null;
        try {
            convertedDate = df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }
    private void selectCalender(final EditText e) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);
                try {
                    if(e.equals(mPrescriptionDate)){
                        mPrescriptionDate.setText(sdf.format(mcurrentDate.getTime()));
                        mPrescriptionDate1 = sdf.parse(sdf.format(mcurrentDate.getTime()));
                    }
                    else  if(e.equals(mPrescriptionExpiryDate)){

                        mPrescriptionExpiryDate.setText(sdf.format(mcurrentDate.getTime()));
                        mExpiryDate = sdf.parse(sdf.format(mcurrentDate.getTime()));
                    }

                } catch (Exception e) {
                    Log.i("DATE", "Date parsing error");
                }
            }
        }, mYear, mMonth, mDay);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }
}