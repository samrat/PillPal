package iss.nus.edu.sg.pillpal;

import iss.nus.edu.sg.pillpal.data.PillPalBaseHelper;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

/**
 * 
 * @author HaNguyen modified by $Author: hanguyen $
 * @version $Revision: 1.2 $ 
 */
public class PillPalBaseHelperTesting extends AndroidTestCase {

    private static PillPalBaseHelper dbHelper;
    private static final String DATABASE_NAME = "pillPal.db";

    @Override
    protected void setUp() throws Exception {
    	super.setUp();
    	dbHelper = new PillPalBaseHelper(mContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        dbHelper = null;
    }
    
    public void testDropDB(){
        assertTrue(mContext.deleteDatabase(DATABASE_NAME));
    }

    public void testCreateDB(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        assertTrue(db.isOpen());
        db.close();
    }
}