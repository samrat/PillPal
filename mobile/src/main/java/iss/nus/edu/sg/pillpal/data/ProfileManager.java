package iss.nus.edu.sg.pillpal.data;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import iss.nus.edu.sg.pillpal.interfaces.IProfileManager;
import iss.nus.edu.sg.pillpal.data.PillPalDbSchema.ProfileTable;
import iss.nus.edu.sg.pillpal.models.Profile;

/**
 * Created by Samrat on 27/5/16.
 */
public class ProfileManager implements IProfileManager{

    /**************************************************/
    // Constants
    /**************************************************/
    private static final String TAG = "ProfileManager";

    /**************************************************/
    // Instance Variables
    /**************************************************/
    private Context mContext;
    private PillPalBaseHelper mPillPalBaseHelper;

    private static ProfileManager sProfileManager;
    /**************************************************/
    // Constructor
    /**************************************************/
    private ProfileManager(Context context) {
        mContext = context.getApplicationContext();
        mPillPalBaseHelper = new PillPalBaseHelper(mContext);
        try {
            mPillPalBaseHelper.createDB();
        }catch (Exception e) {
            Log.d(TAG, "DB not created");
        }
    }

    /**************************************************/
    // Public Methods
    /**************************************************/

    /**
     * Returns the singleton instance of the Profile Manager
     *
     * @param context The current context of the caller
     * @return The instance of the Profile Manager
     */
    public static ProfileManager get(Context context) {
        if (sProfileManager == null) {
            sProfileManager = new ProfileManager(context);
        }
        return sProfileManager;
    }

    /*****************************************************/
    // IProfileManager implementation
    /*****************************************************/
    /**
     * Method will add a profile to the dB.
     * It checks if the combination of name & age already exist in dB. If yes, then it will return null.
     * @param profile The profile that needs to be added to the dB.
     * @return The updated profile that was added to the dB.
     */
    public Profile addProfile(Profile profile) {

        Cursor cursor = null;
        try {
            // First check if the profile already exists!
            mPillPalBaseHelper.openDB();
            String query = getProfileForNameAndAgeCheckQuery(profile);

            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            if( (cursor!= null) && (cursor.getCount() > 0) ) {
                // The profile already exists!! Return null
                return null;
            }else {
                String insertQuery = getInsertQuery(profile);
                // Fire the insert
                mPillPalBaseHelper.getWritableDatabase().execSQL(insertQuery);
                // Time to retrieve the last row
                cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery("SELECT last_insert_rowid();", null);
                try {
                    while (cursor.moveToNext()) {
                        int profileId = cursor.getInt(0);
                        // Return the Profile
                        return getProfile(profileId);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception while getting last updated profileId");
                    e.printStackTrace();
                }
            }
        }catch (Exception e) {
            Log.d(TAG, "Exception while addProfile");
            e.printStackTrace();
        }finally {
            closeDBHelperAndCursor(cursor);
        }

        return null;
    }

    /**
     * Method to get all the profiles present in the dB.
     * @return List of all profile objects that are present in dB.
     *         If there are no objects, then empty arraylist is returned.
     */
    public List<Profile> getAllProfiles() {
        List<Profile> profileList = new ArrayList<>();
        Cursor cursor = null;

        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + ProfileTable.NAME;
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            try {
                while (cursor.moveToNext()) {
                    Profile profile = getProfile(cursor);
                    profileList.add(profile);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getAllProfiles");
                e.printStackTrace();
            }
        }catch (Exception e) {
            Log.d(TAG, "Exception while fetching getAllProfiles");
            e.printStackTrace();
        }finally {
            closeDBHelperAndCursor(cursor);
        }
        return profileList;
    }

    /**
     * Method to get a profile using the profile Id.
     * @param profileId The profile Id depending on which the profile will be returned.
     * @return The profile associated with the provided Id. If not found, then null is returned.
     */
    public Profile getProfile(int profileId) {
        Cursor cursor = null;
        Profile profile = null;
        try {
            mPillPalBaseHelper.openDB();
            String query = "SELECT * FROM " + ProfileTable.NAME + " WHERE " + ProfileTable.Cols.ID + " = " + profileId;
            cursor = mPillPalBaseHelper.getReadableDatabase().rawQuery(query, null);

            try {
                while (cursor.moveToNext()) {
                    profile = getProfile(cursor);
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception while fetching getProfile");
                e.printStackTrace();
            }
        }catch (Exception e) {
            Log.d(TAG, "Exception while fetching getProfile");
            e.printStackTrace();
        }finally {
            closeDBHelperAndCursor(cursor);
        }
        return profile;
    }

    /**
     * Method to remove a profile from the dB.
     * @param profileId The associated profile Id that needs to be removed.
     * @return true if the profile was successfully removed , else false.
     */
    public boolean deleteProfile(int profileId) {

        int deletedRows = 0;
        try {
            mPillPalBaseHelper.openDB();
            deletedRows = mPillPalBaseHelper.getWritableDatabase().delete(ProfileTable.NAME, "id = " + profileId, null);
        }catch (Exception e) {
            Log.d(TAG, "Exception while deleteProfile");
            e.printStackTrace();
        }finally {
            mPillPalBaseHelper.close();
        }

        // Confirm if the row was successfully deleted
        if(deletedRows == 1) {
            return true;
        }

        return false;
    }

    /*****************************************************/
    // Private Methods
    /*****************************************************/

    /**
     * Closes the cursor & db helper.
     * @param cursor The curson that needs to be closed.
     */
    private void closeDBHelperAndCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
        mPillPalBaseHelper.close();
    }

    /**
     * Gets a particular profile from the cursor.
     * @param cursor The cursor from which the profile needs to be returned
     * @return The profile object having all the retrieved values from the cursor.
     *          If an attribute is not found, then it remains null.
     */
    private Profile getProfile(Cursor cursor) {

        Profile profile = new Profile();
        profile.setId(cursor.getInt(cursor.getColumnIndex(ProfileTable.Cols.ID)));
        profile.setName(cursor.getString(cursor.getColumnIndex(ProfileTable.Cols.NAME)));
        profile.setAge(cursor.getInt(cursor.getColumnIndex(ProfileTable.Cols.AGE)));
        profile.setAllergy(cursor.getString(cursor.getColumnIndex(ProfileTable.Cols.ALLERGY)));
        return profile;
    }

    /**
     * Method to create the insert query.
     * @param profile The profile for which the query needs to be created.
     * @return The updated INSERT query.
     */
    private String getInsertQuery(Profile profile) {
        String insertQuery = "INSERT INTO " + ProfileTable.NAME +
                " ('" + ProfileTable.Cols.NAME + "'," +
                "'" + ProfileTable.Cols.AGE + "'," +
                "'" + ProfileTable.Cols.ALLERGY + "')" +
                " VALUES(" +
                "'" + profile.getName() + "'," +
                profile.getAge() + "," +
                "'" + profile.getAllergy() + "'" + ");";
        return insertQuery;
    }

    /**
     * Method to create a select query with where clause for name & age.
     * @param profile The profile for which the query needs to be created.
     * @return The updated SELECT query.
     */
    private String getProfileForNameAndAgeCheckQuery(Profile profile) {
        String query =  "SELECT * FROM " + ProfileTable.NAME + " WHERE " +
                ProfileTable.Cols.NAME + " = " +
                "'" + profile.getName() + "'" +
                " AND " +
                ProfileTable.Cols.AGE + " = " +
                profile.getAge() + ";";
        return query;
    }
}
