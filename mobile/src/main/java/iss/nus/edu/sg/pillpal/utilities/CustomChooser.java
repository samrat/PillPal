package iss.nus.edu.sg.pillpal.utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import iss.nus.edu.sg.pillpal.R;


/**
 * @author Bavithra Thangaraj on 9/8/2016.
 */
public class CustomChooser {

    private CustomChooser.ChooserCallback mChooserCallback_ = null;

    public CustomChooser(ChooserCallback mChooserCallback_) {
        this.mChooserCallback_ = mChooserCallback_;
    }

    private Dialog dialog;
    public  void CustomDialog(final Fragment fragment){

        dialog = new Dialog(fragment.getActivity());
        // it remove the dialog title
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // set the layout in the dialog
        dialog.setContentView(R.layout.inventory_dialog);
        // set the background partial transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        // set the layout at right bottom
        param.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        // it dismiss the dialog when click outside the dialog frame
        dialog.setCanceledOnTouchOutside(true);

        // Setting the actions for the views
        View cameraFloatingIcon = dialog.findViewById(R.id.scan_pill);
        cameraFloatingIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera(fragment);
            }
        });

        View addNewPillFloatingIcon = dialog.findViewById(R.id.enter_pill);
        addNewPillFloatingIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomChooser.this.mChooserCallback_.callback();
                dialog.dismiss();
            }
        });

        // it show the dialog box
        dialog.show();
    }

    public void camera(final Fragment fragment){
        boolean isConnected = UIUtils.isNetworkAvailable(fragment);
        if(isConnected) {
            try {
                Log.v("EnteringinsideOnClick", "EnteringinsideOnClick");
                AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getContext());
                builder
                        .setMessage("Choose a picture")
                        .setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PictureUtility.startGalleryChooser(fragment);
                            }
                        })
                        .setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PictureUtility.startCamera(fragment);
                            }
                        });
                builder.create().show();
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("CameraOpening", e.toString());
                Toast.makeText(fragment.getContext(), "Problem occured in Camera opening", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(fragment.getContext(),"Internet is not connected",Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public interface ChooserCallback{
        public void callback();
    }

}
