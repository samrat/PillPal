package iss.nus.edu.sg.pillpal.scheduler.tasks;

import iss.nus.edu.sg.pillpal.data.PrescriptionManager;
import iss.nus.edu.sg.pillpal.data.ProfileManager;
import iss.nus.edu.sg.pillpal.models.Pill;
import iss.nus.edu.sg.pillpal.models.Prescription;
import iss.nus.edu.sg.pillpal.models.PrescriptionItem;
import iss.nus.edu.sg.pillpal.models.Profile;
import iss.nus.edu.sg.pillpal.services.PillConsumeService;
import iss.nus.edu.sg.pillpal.utilities.PillNotificationUtility;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PillConsumeTask extends CommonTask {
	
	private static final String M_DAY = "Day";
    private static final String M_WEEK = "Week";

    private PrescriptionItem mPrescriptionItem;
    /****************************************************/
    /*
        Business logic for Pill reminder Notification
     */
    /****************************************************/

    @Override
    public void runTask() throws Exception {
        Calendar currentTime = Calendar.getInstance();
        int notifyTime = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        showNotification();
        if((notifyTime == 9 || notifyTime == 16) && minute == 45) {
            showNotification();
        }
    }

    private void showNotification() {
        String message = "";
        ProfileManager pManager = ProfileManager.get(PillConsumeService.context);
        for (Profile profile: pManager.getAllProfiles()) {
            message = checkProfilePrescription(profile);
            if (message != "") {
                PillNotificationUtility.notificationPillConsumption("Pill Reminder", message,mPrescriptionItem);
                break;
            }
        }
    }
    private String checkProfilePrescription(Profile profile) {
        Calendar cal = Calendar.getInstance();
        removeTime(cal);
    	PrescriptionManager presManager = PrescriptionManager.get(PillConsumeService.context);
    	for (Prescription profilePrescription : presManager.getPrescriptionsForProfile(profile.getId())) {
            if (cal.getTime().before(profilePrescription.getPrescriptionExpiryDate())
                || cal.getTime().compareTo(profilePrescription.getPrescriptionExpiryDate()) == 0) {
            	String message = checkPrescription(profilePrescription.getPrescriptionItems(),profile);
            	if (message != "") {
            		return message;
            	}
            }
        }
    	return "";
    }
    
    private String checkPrescription(List<PrescriptionItem> prescriptionItem, Profile profile) {
    	for(PrescriptionItem item : prescriptionItem) {
            if (item.getRoutine().indexOf(M_DAY) >= 0){
                mPrescriptionItem = item;
                if (item.getInstructions().equalsIgnoreCase("Before Lunch") && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < 12){
                    return profile.getName()+" Please take "+ item.getDosage()+ " of " +item.getPill().getName();
                } else if(item.getInstructions().equalsIgnoreCase("After Lunch") && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= 12){
                    return profile.getName()+" Please take "+ item.getDosage()+ " of " +item.getPill().getName();
                }
            } else if ((item.getRoutine().indexOf(M_WEEK) >= 0)) {
                mPrescriptionItem = item;
                if (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                    if (item.getInstructions().equalsIgnoreCase("Before Lunch") && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < 12) {
                        return profile.getName() + " Please take " + item.getDosage() + " of " + item.getPill().getName();
                    } else if (item.getInstructions().equalsIgnoreCase("After Lunch") && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= 12) {
                        return profile.getName() + " Please take " + item.getDosage() + " of " + item.getPill().getName();
                    }
                }
            }
        }
    	return "";
    }


    private void removeTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }
}
