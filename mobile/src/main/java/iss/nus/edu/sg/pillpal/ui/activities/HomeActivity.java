package iss.nus.edu.sg.pillpal.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import iss.nus.edu.sg.pillpal.R;

public class HomeActivity extends BaseAbstractActivity implements View.OnClickListener {

    @Bind(R.id.inventory) LinearLayout mInventory;
    @Bind(R.id.monitor) LinearLayout mMonitor;
    @Bind(R.id.prescription) LinearLayout mPrescription;
    @Bind(R.id.profile) LinearLayout mProfile;
    @Bind(R.id.settings) LinearLayout mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        this.mInventory.setOnClickListener(this);
        this.mMonitor.setOnClickListener(this);
        this.mPrescription.setOnClickListener(this);
        this.mProfile.setOnClickListener(this);
        mSettings.setOnClickListener(this);



        /* Test method called for demo */
//        InventoryManager inventoryManager;
//        inventoryManager = InventoryManager.get(this);
//        inventoryManager.testMethod();
    }

    @Override
    public void onClick(View v) {
        if (v == this.mInventory) {
            Intent intent = new Intent(this, InventoryActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (v == this.mMonitor) {
            Intent intent = new Intent(this, MonitorActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (v == this.mPrescription) {
            Intent intent = new Intent(this, PrescriptionActivity.class);
            intent.putExtra("Prescription","Prescription");
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (v == this.mProfile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("Profile","Profile");
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (v == this.mSettings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

}
